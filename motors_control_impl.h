/**
 * @file      motors_control_impl.h
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-Quatro (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _MOTORS_CONTROL_IMPL_H_
#define	_MOTORS_CONTROL_IMPL_H_

#include <xc.h>
#include <stdint.h>
#include "config/motors_control_config.h"

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */
   
    extern int16_t(*motors_control_position_sensor[MOTORS_CONTROL_MOTOR_COUNT])(void);
    extern void (*motors_control_position_preset[MOTORS_CONTROL_MOTOR_COUNT])(int16_t value);
    extern void (*motors_control_L298_forward[MOTORS_CONTROL_MOTOR_COUNT])(uint16_t duty);
    extern void (*motors_control_L298_reverse[MOTORS_CONTROL_MOTOR_COUNT])(uint16_t duty);
    extern void (*motors_control_L298_brake[MOTORS_CONTROL_MOTOR_COUNT])(uint16_t duty);

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* _MOTORS_CONTROL_IMPL_H_ */

