/**
 * @file      serial_config.h
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-PID (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _SERIAL_CONFIG_H_
#define	_SERIAL_CONFIG_H_

#include <xc.h>

#define SERIAL_SETUP_ANSEL() do{ANSELC &= ~0b11000000;}while(0)
#define SERIAL_TX_TRIS TRISCbits.TRISC6
#define SERIAL_RX_TRIS TRISCbits.TRISC7

#define SERIAL_TXSTA TXSTA1
#define SERIAL_RCSTA RCSTA1
#define SERIAL_BAUDCON BAUDCON1
#define SERIAL_SPBRG SPBRG1
#define SERIAL_TXIP TX1IP
#define SERIAL_TXIF TX1IF
#define SERIAL_TXIE TX1IE
#define SERIAL_RCIP RC1IP
#define SERIAL_RCIF RC1IF
#define SERIAL_RCIE RC1IE
#define SERIAL_RCREG RCREG1
#define SERIAL_TXREG TXREG1

#endif	/* _SERIAL_CONFIG_H_ */
