/**
 * @file      i2c_master_config.h
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-PID (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _I2C_MASTER_CONFIG_H_
#define _I2C_MASTER_CONFIG_H_

#include <xc.h>

#ifndef _XTAL_FREQ
#define _XTAL_FREQ 32000000UL
#endif

#ifndef FSCL
#define FSCL 100000UL
#endif

#define I2C_SCL_LAT LATCbits.LATC3
#define I2C_SDA_LAT LATCbits.LATC4
#define I2C_SCL_TRIS TRISCbits.TRISC3
#define I2C_SDA_TRIS TRISCbits.TRISC4
#define I2C_SCL_PORT PORTCbits.RC3
#define I2C_SDA_PORT PORTCbits.RC4
#define I2C_SETUP_ANSEL() do{ANSELCbits.ANSC3=0;ANSELCbits.ANSC4=0;}while(0)

#define SSPREG_STAT SSPSTAT
#define SSPREG_ADD SSPADD
#define SSPREG_CON1 SSPCON1
#define SSPREG_CON2 SSPCON2

#define SSP_IF SSPIF
#define SSP_SEN SSP1CON2bits.SEN
#define SSP_PEN SSP1CON2bits.PEN
#define SSP_RSEN SSP1CON2bits.RSEN
#define SSP_SSPEN SSP1CON1bits.SSPEN
#define SSP_SSPOV SSP1CON1bits.SSPOV
#define SSP_RCEN SSP1CON2bits.RCEN
#define SSP_ACKEN SSP1CON2bits.ACKEN
#define SSP_WCOL SSP1CON1bits.WCOL
#define SSP_BF SSP1STATbits.BF
#define SSP_ACKSTAT SSP1CON2bits.ACKSTAT
#define SSP_ACKDT SSP1CON2bits.ACKDT
#define SSP_BUF SSP1BUF

#endif	/* _I2C_MASTER_CONFIG_CONFIG_H_ */

