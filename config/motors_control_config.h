/**
 * @file      motors_control_config.h
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-PID (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _MOTORS_CONTROL_CONFIG_H_
#define	_MOTORS_CONTROL_CONFIG_H_

#include <xc.h>
#include "../sys_clock.h"

#define MOTORS_CONTROL_MOTOR_COUNT 2
// 1/16 pre-scaler
// 1/2 post-scaler
#define MOTORS_CONTROL_T4CON   0b00001010
#define MOTORS_CONTROL_FREQ    ((_XTAL_FREQ)/4UL/2UL/256UL/16UL)
#define MOTORS_CONTROL_DELTA_T ((float)(4UL * 2UL * 256UL * 16UL) / (float)(_XTAL_FREQ))


#endif	/* _MOTORS_CONTROL_CONFIG_H_ */