/**
 * @file      encoder_config.h
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-PID (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _ENCODER_CONFIG_H_
#define	_ENCODER_CONFIG_H_

#include <xc.h>

#define ENCODER_PORT PORTB

#define ENCODER_0_DIR  1
#define ENCODER_1_DIR -1

#define ENCODER_0_A_INTIE INT2IE
#define ENCODER_0_A_INTIF INT2IF
#define ENCODER_0_A_INTIP(x) do{INT2IP=x;}while(0)
#define ENCODER_0_A 0x04
#define ENCODER_0_B 0x10
#define ENCODER_1_A_INTIE INT1IE
#define ENCODER_1_A_INTIF INT1IF
#define ENCODER_1_A_INTIP(x) do{INT1IP=x;}while(0)
#define ENCODER_1_A 0x02
#define ENCODER_1_B 0x08
#define ENCODER_SETUP_ANSEL() do{ANSELB&=~(ENCODER_0_A|ENCODER_0_B|ENCODER_1_A|ENCODER_1_B);}while(0)

#endif	/* _ENCODER_CONFIG_H_ */

