/**
 * @file      bmx055.c
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-PID (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */


#include "sys_clock.h"
#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include "i2c_master.h"
#include "bmx055.h"


#ifndef BMX055_DEV_ADDR_ACCELERATION
#define BMX055_DEV_ADDR_ACCELERATION 0x19
#endif

#ifndef BMX055_DEV_ADDR_GYROSCOPE
#define BMX055_DEV_ADDR_GYROSCOPE    0x69
#endif

#ifndef BMX055_DEV_ADDR_GEOMAGNETIC
#define BMX055_DEV_ADDR_GEOMAGNETIC  0x13
#endif

#define BMX055_ACC_REG_OFFSET_BGW_CHIPID     0x00
#define BMX055_ACC_REG_OFFSET_ACCD_X_LSB     0x02
#define BMX055_ACC_REG_OFFSET_ACCD_X_MSB     0x03
#define BMX055_ACC_REG_OFFSET_ACCD_Y_LSB     0x04
#define BMX055_ACC_REG_OFFSET_ACCD_Y_MSB     0x05
#define BMX055_ACC_REG_OFFSET_ACCD_Z_LSB     0x06
#define BMX055_ACC_REG_OFFSET_ACCD_Z_MSB     0x07
#define BMX055_ACC_REG_OFFSET_ACCD_TEMP      0x08
#define BMX055_ACC_REG_OFFSET_INT_STATUS_0   0x09
#define BMX055_ACC_REG_OFFSET_INT_STATUS_1   0x0A
#define BMX055_ACC_REG_OFFSET_INT_STATUS_2   0x0B
#define BMX055_ACC_REG_OFFSET_INT_STATUS_3   0x0C
#define BMX055_ACC_REG_OFFSET_FIFO_STATUS    0x0E
#define BMX055_ACC_REG_OFFSET_PMU_RANGE      0x0F
#define BMX055_ACC_REG_OFFSET_PMU_BW         0x10
#define BMX055_ACC_REG_OFFSET_PMU_LPW        0x11
#define BMX055_ACC_REG_OFFSET_PMU_LOW_POWER  0x12
#define BMX055_ACC_REG_OFFSET_ACCD_HBW       0x13
#define BMX055_ACC_REG_OFFSET_BGW_SOFTRESET  0x14
#define BMX055_ACC_REG_OFFSET_INT_EN_0       0x16
#define BMX055_ACC_REG_OFFSET_INT_EN_1       0x17
#define BMX055_ACC_REG_OFFSET_INT_EN_2       0x18
#define BMX055_ACC_REG_OFFSET_INT_MAP_0      0x19
#define BMX055_ACC_REG_OFFSET_INT_MAP_1      0x1A
#define BMX055_ACC_REG_OFFSET_INT_MAP_2      0x1B
#define BMX055_ACC_REG_OFFSET_INT_SRC        0x1E
#define BMX055_ACC_REG_OFFSET_INT_OUT_CTRL   0x20
#define BMX055_ACC_REG_OFFSET_INT_RST_LATCH  0x21
#define BMX055_ACC_REG_OFFSET_INT_0          0x22
#define BMX055_ACC_REG_OFFSET_INT_1          0x23
#define BMX055_ACC_REG_OFFSET_INT_2          0x24
#define BMX055_ACC_REG_OFFSET_INT_3          0x25
#define BMX055_ACC_REG_OFFSET_INT_4          0x26
#define BMX055_ACC_REG_OFFSET_INT_5          0x27
#define BMX055_ACC_REG_OFFSET_INT_6          0x28
#define BMX055_ACC_REG_OFFSET_INT_7          0x29
#define BMX055_ACC_REG_OFFSET_INT_8          0x2A
#define BMX055_ACC_REG_OFFSET_INT_9          0x2B
#define BMX055_ACC_REG_OFFSET_INT_A          0x2C
#define BMX055_ACC_REG_OFFSET_INT_B          0x2D
#define BMX055_ACC_REG_OFFSET_INT_C          0x2E
#define BMX055_ACC_REG_OFFSET_INT_D          0x2F
#define BMX055_ACC_REG_OFFSET_FIFO_CONFIG_0  0x30
#define BMX055_ACC_REG_OFFSET_PMU_SELF_TEST  0x32
#define BMX055_ACC_REG_OFFSET_TRIM_NVM_CTRL  0x33
#define BMX055_ACC_REG_OFFSET_BGW_SPI3_WDT   0x34
#define BMX055_ACC_REG_OFFSET_OFC_CTRL       0x36
#define BMX055_ACC_REG_OFFSET_OFC_SETTING    0x37
#define BMX055_ACC_REG_OFFSET_OFC_X          0x38
#define BMX055_ACC_REG_OFFSET_OFC_Y          0x39
#define BMX055_ACC_REG_OFFSET_OFC_Z          0x3A
#define BMX055_ACC_REG_OFFSET_TRIM_GP0       0x3B
#define BMX055_ACC_REG_OFFSET_TRIM_GP1       0x3C
#define BMX055_ACC_REG_OFFSET_FIFO_CONFIG_1  0x3E
#define BMX055_ACC_REG_OFFSET_FIFO_DATA      0x3F

#define BMX055_GYR_REG_OFFSET_CHIP_ID        0x00
#define BMX055_GYR_REG_OFFSET_RATE_X_LSB     0x02
#define BMX055_GYR_REG_OFFSET_RATE_X_MSB     0x03
#define BMX055_GYR_REG_OFFSET_RATE_Y_LSB     0x04
#define BMX055_GYR_REG_OFFSET_RATE_Y_MSB     0x05
#define BMX055_GYR_REG_OFFSET_RATE_Z_LSB     0x06
#define BMX055_GYR_REG_OFFSET_RATE_Z_MSB     0x07
#define BMX055_GYR_REG_OFFSET_INT_STATUS_0   0x09
#define BMX055_GYR_REG_OFFSET_INT_STATUS_1   0x0A
#define BMX055_GYR_REG_OFFSET_INT_STATUS_2   0x0B
#define BMX055_GYR_REG_OFFSET_INT_STATUS_3   0x0C
#define BMX055_GYR_REG_OFFSET_FIFO_STATUS    0x0E
#define BMX055_GYR_REG_OFFSET_RAGE           0x0F
#define BMX055_GYR_REG_OFFSET_BW             0x10
#define BMX055_GYR_REG_OFFSET_LPM1           0x11
#define BMX055_GYR_REG_OFFSET_LPM2           0x12
#define BMX055_GYR_REG_OFFSET_RATE_HBW       0x13
#define BMX055_GYR_REG_OFFSET_BGW_SOFTRESET  0x14
#define BMX055_GYR_REG_OFFSET_INT_EN_0       0x15
#define BMX055_GYR_REG_OFFSET_INT_EN_1       0x16
#define BMX055_GYR_REG_OFFSET_INT_MAP_0      0x17
#define BMX055_GYR_REG_OFFSET_INT_MAP_1      0x18
#define BMX055_GYR_REG_OFFSET_INT_MAP_2      0x19
#define BMX055_GYR_REG_OFFSET_INT_RST_LATCH  0x21
#define BMX055_GYR_REG_OFFSET_HIGH_TH_X      0x22
#define BMX055_GYR_REG_OFFSET_HIGH_DUR_X     0x23
#define BMX055_GYR_REG_OFFSET_HIGH_TH_Y      0x24
#define BMX055_GYR_REG_OFFSET_HIGH_DUR_Y     0x25
#define BMX055_GYR_REG_OFFSET_HIGH_TH_Z      0x26
#define BMX055_GYR_REG_OFFSET_HIGH_DUR_Z     0x27
#define BMX055_GYR_REG_OFFSET_SOC            0x31
#define BMX055_GYR_REG_OFFSET_A_FOC          0x32
#define BMX055_GYR_REG_OFFSET_TRIM_NVM_CTRL  0x33
#define BMX055_GYR_REG_OFFSET_BGW_SPI3_WDT   0x34
#define BMX055_GYR_REG_OFFSET_OFC1           0x36
#define BMX055_GYR_REG_OFFSET_OFC2           0x37
#define BMX055_GYR_REG_OFFSET_OFC3           0x38
#define BMX055_GYR_REG_OFFSET_OFC4           0x39
#define BMX055_GYR_REG_OFFSET_TRIM_GP0       0x3A
#define BMX055_GYR_REG_OFFSET_TRIM_GP1       0x3B
#define BMX055_GYR_REG_OFFSET_BIST           0x3C
#define BMX055_GYR_REG_OFFSET_FIFO_CONFIG_0  0x3D
#define BMX055_GYR_REG_OFFSET_FIFO_CONFIG_1  0x3E
#define BMX055_GYR_REG_OFFSET_FIFO_DATA      0x3F

#define BMX055_MAG_REG_OFFSET_CHIP_ID        0x40
#define BMX055_MAG_REG_OFFSET_DATAX_LSB      0x42
#define BMX055_MAG_REG_OFFSET_DATAX_MSB      0x43
#define BMX055_MAG_REG_OFFSET_DATAY_LSB      0x44
#define BMX055_MAG_REG_OFFSET_DATAY_MSB      0x45
#define BMX055_MAG_REG_OFFSET_DATAZ_LSB      0x46
#define BMX055_MAG_REG_OFFSET_DATAZ_MSB      0x47
#define BMX055_MAG_REG_OFFSET_RHALL_LSB      0x48
#define BMX055_MAG_REG_OFFSET_RHALL_MSB      0x49
#define BMX055_MAG_REG_OFFSET_INT_STATUS     0x4A
#define BMX055_MAG_REG_OFFSET_POW_CTL_REG    0x4B
#define BMX055_MAG_REG_OFFSET_OPMODE         0x4C
#define BMX055_MAG_REG_OFFSET_INT_SETTING_0  0x4D
#define BMX055_MAG_REG_OFFSET_INT_SETTING_1  0x4E
#define BMX055_MAG_REG_OFFSET_LOW_THR_INT    0x4F
#define BMX055_MAG_REG_OFFSET_HIGH_THR_INT   0x50
#define BMX055_MAG_REG_OFFSET_REPXY          0x51
#define BMX055_MAG_REG_OFFSET_REPZ           0x52

int BMX055_init(void) {
    int ret;
    uint8_t buf;
    /* Detect slave devices on BMX055 */
    if (I2C_MASTER_RET_OK != (ret = I2C_MASTER_poll(BMX055_DEV_ADDR_ACCELERATION))) {
        return EOF;
    }
    if (I2C_MASTER_RET_OK != I2C_MASTER_poll(BMX055_DEV_ADDR_GYROSCOPE)) {
        return EOF;
    }
    if (I2C_MASTER_RET_OK != I2C_MASTER_poll(BMX055_DEV_ADDR_GEOMAGNETIC)) {
        return EOF;
    }
    /* SoftReset */
    buf = 0xB6;
    if (I2C_MASTER_RET_OK != I2C_MASTER_write(BMX055_DEV_ADDR_ACCELERATION, BMX055_ACC_REG_OFFSET_BGW_SOFTRESET, 1, &buf)) {
        return EOF;
    }
    buf = 0xB6;
    if (I2C_MASTER_RET_OK != I2C_MASTER_write(BMX055_DEV_ADDR_GYROSCOPE, BMX055_GYR_REG_OFFSET_BGW_SOFTRESET, 1, &buf)) {
        return EOF;
    }
    buf = 0x82;
    if (I2C_MASTER_RET_OK != I2C_MASTER_write(BMX055_DEV_ADDR_GEOMAGNETIC, BMX055_MAG_REG_OFFSET_POW_CTL_REG, 1, &buf)) {
        return EOF;
    }
    __delay_ms(100);
    /* Acceleration Setting */
    // PMU Range : +-2
    buf = 0b0011;
    if (I2C_MASTER_RET_OK != I2C_MASTER_write(BMX055_DEV_ADDR_ACCELERATION, BMX055_ACC_REG_OFFSET_PMU_RANGE, 1, &buf)) {
        return EOF;
    }
    // PMU BW : 7.81Hz
    buf = 0b01000;
    if (I2C_MASTER_RET_OK != I2C_MASTER_write(BMX055_DEV_ADDR_ACCELERATION, BMX055_ACC_REG_OFFSET_PMU_BW, 1, &buf)) {
        return EOF;
    }
    // PMU LPW :  NomalMode, SleepDuration 0.5ms
    buf = 0;
    if (I2C_MASTER_RET_OK != I2C_MASTER_write(BMX055_DEV_ADDR_ACCELERATION, BMX055_ACC_REG_OFFSET_PMU_LPW, 1, &buf)) {
        return EOF;
    }
    /* Gyroscope Setting */
    // Gyro Range : 262.4 LSB/°/s
    buf = 0b100;
    if (I2C_MASTER_RET_OK != I2C_MASTER_write(BMX055_DEV_ADDR_GYROSCOPE, BMX055_GYR_REG_OFFSET_RAGE, 1, &buf)) {
        return EOF;
    }
    // Gyro BW : 32Hz
    buf = 0b0111;
    if (I2C_MASTER_RET_OK != I2C_MASTER_write(BMX055_DEV_ADDR_GYROSCOPE, BMX055_GYR_REG_OFFSET_BW, 1, &buf)) {
        return EOF;
    }
    // Gyro LPM1 : NomalMode, SleepDuration 2ms
    buf = 0;
    if (I2C_MASTER_RET_OK != I2C_MASTER_write(BMX055_DEV_ADDR_GYROSCOPE, BMX055_GYR_REG_OFFSET_LPM1, 1, &buf)) {
        return EOF;
    }
    /* Geomagnetic Setting */
    // set sleep mode
    buf = 0x01;
    if (I2C_MASTER_RET_OK != I2C_MASTER_write(BMX055_DEV_ADDR_GEOMAGNETIC, BMX055_MAG_REG_OFFSET_POW_CTL_REG, 1, &buf)) {
        return EOF;
    }
    __delay_ms(100);
    // adv.st, DataRate, OperationMode, SelfTest (NomalMode, ODR 10Hz)
    buf = 0x00;
    if (I2C_MASTER_RET_OK != I2C_MASTER_write(BMX055_DEV_ADDR_GEOMAGNETIC, BMX055_MAG_REG_OFFSET_OPMODE, 1, &buf)) {
        return EOF;
    }
    // Repetitions for X-Y Axis  0x04 -> 0b00000100 -> (1+2(2^2)) = 9
    buf = 0x04;
    if (I2C_MASTER_RET_OK != I2C_MASTER_write(BMX055_DEV_ADDR_GEOMAGNETIC, BMX055_MAG_REG_OFFSET_REPXY, 1, &buf)) {
        return EOF;
    }
    // Repetitions for Z-Axis  0x0F-> 0b00001111-> (1 +(2^0 + 2^1 + 2^2 + 2^3) = 15
    buf = 0x0F;
    if (I2C_MASTER_RET_OK != I2C_MASTER_write(BMX055_DEV_ADDR_GEOMAGNETIC, BMX055_MAG_REG_OFFSET_REPZ, 1, &buf)) {
        return EOF;
    }
    while (1) {
        if (I2C_MASTER_RET_OK != I2C_MASTER_read(BMX055_DEV_ADDR_GEOMAGNETIC, BMX055_MAG_REG_OFFSET_RHALL_LSB, 1, &buf)) {
            return EOF;
        } else if (buf & 0x01) {
            break;
        }
    }
    (void) buf;
    return 0;
}

int BMX0255_read_ACC_raw(uint8_t xyz[6]) {
    return (I2C_MASTER_RET_OK != I2C_MASTER_read(BMX055_DEV_ADDR_ACCELERATION, BMX055_ACC_REG_OFFSET_ACCD_X_LSB, 6, xyz));
}

int BMX0255_read_GYR_raw(uint8_t xyz[6]) {
    return (I2C_MASTER_RET_OK != I2C_MASTER_read(BMX055_DEV_ADDR_GYROSCOPE, BMX055_GYR_REG_OFFSET_RATE_X_LSB, 6, xyz));
}

int BMX0255_read_MAG_raw(uint8_t xyz[6]) {
    return (I2C_MASTER_RET_OK != I2C_MASTER_read(BMX055_DEV_ADDR_GEOMAGNETIC, BMX055_MAG_REG_OFFSET_DATAX_LSB, 6, xyz));
}

int BMX055_read(BMX055_Raw* raw) {
    if (BMX0255_read_ACC_raw((uint8_t*) & raw->ACC)) {
        return EOF;
    }
    if (BMX0255_read_GYR_raw((uint8_t*) & raw->GYR)) {
        return EOF;
    }
    if (BMX0255_read_MAG_raw((uint8_t*) & raw->MAG)) {
        return EOF;
    }
    return 0;
}

void BMX055_convert(BMX055_Data* dest, const BMX055_Raw* src) {
    dest->ACC.X = ((int16_t) src->ACC.X) >> 4;
    dest->ACC.Y = ((int16_t) src->ACC.Y) >> 4;
    dest->ACC.Z = ((int16_t) src->ACC.Z) >> 4;
    dest->GYR.X = ((int16_t) src->GYR.X);
    dest->GYR.Y = ((int16_t) src->GYR.Y);
    dest->GYR.Z = ((int16_t) src->GYR.Z);
    dest->MAG.X = ((int16_t) src->MAG.X) >> 3;
    dest->MAG.Y = ((int16_t) src->MAG.Y) >> 3;
    dest->MAG.Z = ((int16_t) src->MAG.Z) >> 1;
}