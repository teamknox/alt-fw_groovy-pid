/**
 * @file      motors_control_impl.c
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-PID (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <xc.h>
#include "encoder.h"
#include "L298.h"
#include "motors_control_impl.h"

int16_t(*motors_control_position_sensor[MOTORS_CONTROL_MOTOR_COUNT])(void) = {
    encoder_0_value, encoder_1_value
};

void (*motors_control_position_preset[MOTORS_CONTROL_MOTOR_COUNT])(int16_t value) = {
    encoder_0_preset, encoder_1_preset
};

void (*motors_control_L298_forward[MOTORS_CONTROL_MOTOR_COUNT])(uint16_t duty) = {
    L298_0_forward, L298_1_forward
};

void (*motors_control_L298_reverse[MOTORS_CONTROL_MOTOR_COUNT])(uint16_t duty) = {
    L298_0_reverse, L298_1_reverse
};

void (*motors_control_L298_brake[MOTORS_CONTROL_MOTOR_COUNT])(uint16_t duty) = {
    L298_0_brake, L298_1_brake
};
