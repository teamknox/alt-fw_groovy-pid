/**
 * @file      communication.c
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-PID (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "info.h"
#include "led.h"
#include "serial.h"
#include "parameter.h"
#include "communication.h"
#include "motors_control.h"
#include "bmx055.h"
#include "MS_lib/core/microshell.h"
#include "MS_lib/core/msconf.h"
#include "MS_lib/util/mscmd.h"
#include "SeriCon_lib/SeriCon.h"

static int8_t mode_sericon_or_not_microshell = 0;

#define SERICON_TX_BUFFER_SIZE 32
#define SERICON_RX_BUFFER_SIZE 32

static SeriCon sericon;
static uint8_t sericon_tx_buffer[SERICON_TX_BUFFER_SIZE];
static uint8_t sericon_rx_buffer[SERICON_RX_BUFFER_SIZE];

#ifdef SUPPORT_I2C
static uint8_t BMX055_found = 0;
static BMX055_Data BMX055_data = {0};
#endif

static const SeriConHAL sericon_hal = {
    motors_control_get_isr_event_couter,
    serial_get,
    serial_put
};

static int sericon_handler(SeriCon *sc, size_t sz, const uint8_t *rx) {
    uint8_t* tx = SeriCon_getTxBuffer(sc);
    int tx_sz = 0;
    led_toggle();
    tx[0] = 0x80 | rx[0];
    tx[1] = 0; // event flag
    switch (rx[0]) {
        case 0x00:
        { // VERSION
            tx[2] = (uint8_t) (0xFF & (SOFTWARE_VERSION >> 24));
            tx[3] = (uint8_t) (0xFF & (SOFTWARE_VERSION >> 16));
            tx[4] = (uint8_t) (0xFF & (SOFTWARE_VERSION >> 8));
            tx[5] = (uint8_t) (0xFF & (SOFTWARE_VERSION >> 0));
            tx_sz = 6;
        }
            break;
        case 0x01:
        { // Current mode
            motors_control_feedback();
            int i;
            for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
                tx[2 + i] = motors_control_get_mode(i);
            }
            tx_sz = 2 + MOTORS_CONTROL_MOTOR_COUNT;
        }
            break;
        case 0x02:
        { // Demand Position
            motors_control_feedback();
            int i;
            for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
                volatile int16_t *value = (volatile int16_t *) & tx[2 + 2 * i];
                *value = motors_control_get_demand_position(i);
            }
            tx_sz = 2 + 2 * MOTORS_CONTROL_MOTOR_COUNT;
        }
            break;
        case 0x03:
        { // Actual Position
            motors_control_feedback();
            int i;
            for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
                volatile int16_t *value = (volatile int16_t *) & tx[2 + 2 * i];
                *value = motors_control_get_actual_position(i);
            }
            tx_sz = 2 + 2 * MOTORS_CONTROL_MOTOR_COUNT;
        }
            break;
        case 0x04:
        { // Demand PWM
            motors_control_feedback();
            int i;
            for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
                volatile int16_t *value = (volatile int16_t *) & tx[2 + 2 * i];
                *value = motors_control_get_demand_pwm(i);
            }
            tx_sz = 2 + 2 * MOTORS_CONTROL_MOTOR_COUNT;
        }
            break;
#ifdef SUPPORT_I2C
        case 0x05:
        { // BMX055
            tx[1] = BMX055_found;
            volatile int16_t *value = (volatile int16_t *) & tx[2];
            *value = BMX055_data.ACC.X;
            value++;
            *value = BMX055_data.ACC.Y;
            value++;
            *value = BMX055_data.ACC.Z;
            value++;
            *value = BMX055_data.GYR.X;
            value++;
            *value = BMX055_data.GYR.Y;
            value++;
            *value = BMX055_data.GYR.Z;
            value++;
            *value = BMX055_data.MAG.X;
            value++;
            *value = BMX055_data.MAG.Y;
            value++;
            *value = BMX055_data.MAG.Z;
            tx_sz = 2 + 2 * 9;
        }
            break;
#endif
        case 0x10: // COMMAND
        case 0x11: // COMMAND FB Actual Position
        case 0x12: // COMMAND FB Actual Position, BMX055
        {
            int n = (sz - 2) / 2;
            if ((MOTORS_CONTROL_MOTOR_COUNT == n) && (!(sz & 1))) {
                uint8_t mode = rx[1];
                int i;
                for (i = 0; i < n; i++) {
                    volatile int16_t *value = (volatile int16_t *) & rx[2 + 2 * i];
                    motors_control_set_command(i, mode, *value);
                }
                tx_sz = 2;
                if (rx[0] >= 0x11) {
                    motors_control_feedback();
                    for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
                        volatile int16_t *value = (volatile int16_t *) & tx[tx_sz + 2 * i];
                        *value = motors_control_get_actual_position(i);
                    }
                    tx_sz += 2 * MOTORS_CONTROL_MOTOR_COUNT;
                }
#ifdef SUPPORT_I2C
                if (rx[0] >= 0x12) {
                    tx[1] = BMX055_found;
                    volatile int16_t *value = (volatile int16_t *) & tx[tx_sz];
                    *value = BMX055_data.ACC.X;
                    value++;
                    *value = BMX055_data.ACC.Y;
                    value++;
                    *value = BMX055_data.ACC.Z;
                    value++;
                    *value = BMX055_data.GYR.X;
                    value++;
                    *value = BMX055_data.GYR.Y;
                    value++;
                    *value = BMX055_data.GYR.Z;
                    value++;
                    *value = BMX055_data.MAG.X;
                    value++;
                    *value = BMX055_data.MAG.Y;
                    value++;
                    *value = BMX055_data.MAG.Z;
                    tx_sz += 2 * 9;
                }
#endif
            } else {
                tx_sz = 2;
            }
        }
            break;
        case 0x20: // EXECUTE
        case 0x21: // EXECUTE FB Actual Position
        case 0x22: // EXECUTE FB Actual Position, BMX055
        {
            motors_control_execute();
            tx_sz = 2;
            if (rx[0] >= 0x21) {
                int i;
                motors_control_feedback();
                for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
                    volatile int16_t *value = (volatile int16_t *) & tx[tx_sz + 2 * i];
                    *value = motors_control_get_actual_position(i);
                }
                tx_sz += 2 * MOTORS_CONTROL_MOTOR_COUNT;
            }
#ifdef SUPPORT_I2C
            if (rx[0] >= 0x22) {
                tx[1] = BMX055_found;
                volatile int16_t *value = (volatile int16_t *) & tx[tx_sz];
                *value = BMX055_data.ACC.X;
                value++;
                *value = BMX055_data.ACC.Y;
                value++;
                *value = BMX055_data.ACC.Z;
                value++;
                *value = BMX055_data.GYR.X;
                value++;
                *value = BMX055_data.GYR.Y;
                value++;
                *value = BMX055_data.GYR.Z;
                value++;
                *value = BMX055_data.MAG.X;
                value++;
                *value = BMX055_data.MAG.Y;
                value++;
                *value = BMX055_data.MAG.Z;
                tx_sz += 2 * 9;
            }
#endif
        }
            break;
        case 0x30: // COMMAND & EXECUTE
        case 0x31: // COMMAND & EXECUTE FB Actual Position
        case 0x32: // COMMAND & EXECUTE FB Actual Position, BMX055
        {
            int n = (sz - 2) / 2;
            if ((MOTORS_CONTROL_MOTOR_COUNT == n) && (!(sz & 1))) {
                uint8_t mode = rx[1];
                int i;
                for (i = 0; i < n; i++) {
                    volatile int16_t *value = (volatile int16_t *) & rx[2 + 2 * i];
                    motors_control_set_command(i, mode, *value);
                }
                motors_control_execute();
                tx_sz = 2;
                if (rx[0] >= 0x31) {
                    int i;
                    motors_control_feedback();
                    for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
                        volatile int16_t *value = (volatile int16_t *) & tx[tx_sz + 2 * i];
                        *value = motors_control_get_actual_position(i);
                    }
                    tx_sz += 2 * MOTORS_CONTROL_MOTOR_COUNT;
                }
#ifdef SUPPORT_I2C
                if (rx[0] >= 0x32) {
                    tx[1] = BMX055_found;
                    volatile int16_t *value = (volatile int16_t *) & tx[tx_sz];
                    *value = BMX055_data.ACC.X;
                    value++;
                    *value = BMX055_data.ACC.Y;
                    value++;
                    *value = BMX055_data.ACC.Z;
                    value++;
                    *value = BMX055_data.GYR.X;
                    value++;
                    *value = BMX055_data.GYR.Y;
                    value++;
                    *value = BMX055_data.GYR.Z;
                    value++;
                    *value = BMX055_data.MAG.X;
                    value++;
                    *value = BMX055_data.MAG.Y;
                    value++;
                    *value = BMX055_data.MAG.Z;
                    tx_sz += 2 * 9;
                }
#endif
            } else {
                tx_sz = 2;
            }
        }
            break;
        case 0x7F:
        { // enter microshell mode
            mode_sericon_or_not_microshell = 0;
            tx_sz = 2;
        }
            break;
        default:
            tx_sz = 2;
            break;
    }
    if (tx_sz > 0) {
        return SeriCon_send(sc, tx_sz, tx);
    } else {
        return 0;
    }
}

MICROSHELL microshell;
MSCMD mscmd;
static uint8_t shell_echo = 0;
uint8_t microshell_buffer_idx = 0;
char microshell_buffer[MSCONF_MAX_INPUT_LENGTH];

static void println(void) {
    printf("\r\n");
}

static void microshell_tx(char c) {
    putch(c);
}

static char microshell_rx(void) {
    int ret = EOF;
    while (EOF == (ret = serial_get())) {
        Nop();
    }
    return ret;
}

static void microshell_action_hook(MSCORE_ACTION action) {
}

static MSCMD_USER_RESULT usrcmd_LED(MSOPT *msopt, MSCMD_USER_OBJECT usrobj) {
    int argc;
    msopt_get_argc(msopt, &argc);
    do {
        if (0) {
        } else if (1 == argc) {
            printf("%d", led_state());
            break;
        } else if (2 == argc) {
            char buf[MSCONF_MAX_INPUT_LENGTH];
            msopt_get_argv(msopt, 1, buf, sizeof (buf));
            if ('0' == buf[0]) {
                led_off();
                printf("OK");
                break;
            } else if ('1' == buf[0]) {
                led_on();
                printf("OK");
                break;
            }
        }
        printf("NG");
    } while (0);
    return 0;
}

static MSCMD_USER_RESULT usrcmd_Command(MSOPT *msopt, MSCMD_USER_OBJECT usrobj) {
    int argc;
    msopt_get_argc(msopt, &argc);
    do {
        if (0) {
        } else if (4 == argc) {
            char buf[MSCONF_MAX_INPUT_LENGTH];
            msopt_get_argv(msopt, 1, buf, sizeof (buf));
            int idx = atoi(buf);
            msopt_get_argv(msopt, 2, buf, sizeof (buf));
            int mode = strtoul(buf, NULL, 16);
            msopt_get_argv(msopt, 3, buf, sizeof (buf));
            int target = atoi(buf);
            motors_control_set_command(idx, mode, target);
            printf("OK");
            break;
        }
        printf("NG");
    } while (0);
    return 0;
}

static MSCMD_USER_RESULT usrcmd_Execute(MSOPT *msopt, MSCMD_USER_OBJECT usrobj) {
    motors_control_execute();
    printf("OK");
    return 0;
}

static MSCMD_USER_RESULT usrcmd_ExecuteImmediate(MSOPT *msopt, MSCMD_USER_OBJECT usrobj) {
    int argc;
    msopt_get_argc(msopt, &argc);
    do {
        if (0) {
        } else if ((2 + MOTORS_CONTROL_MOTOR_COUNT) == argc) {
            char buf[MSCONF_MAX_INPUT_LENGTH];
            msopt_get_argv(msopt, 1, buf, sizeof (buf));
            int mode = strtoul(buf, NULL, 16);
            int i;
            for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
                msopt_get_argv(msopt, 2 + i, buf, sizeof (buf));
                int target = atoi(buf);
                motors_control_set_command(i, mode, target);
            }
            motors_control_execute();
            printf("OK");
            break;
        }
        printf("NG");
    } while (0);
    return 0;
}

static MSCMD_USER_RESULT usrcmd_Parameter(MSOPT *msopt, MSCMD_USER_OBJECT usrobj) {
    int argc;
    msopt_get_argc(msopt, &argc);
    do {
        if (0) {
        } else if (1 == argc) {
            printf("pos_error_min=%d", motors_control_get_position_parameter_error_min());
            println();
            printf("pos_error_max=%d", motors_control_get_position_parameter_error_max());
            println();
            printf("pos_ignore_min=%d", motors_control_get_position_parameter_ignore_min());
            println();
            printf("pos_ignore_max=%d", motors_control_get_position_parameter_ignore_max());
            println();
            printf("pos_hysteresis_min=%d", motors_control_get_position_parameter_hysteresis_min());
            println();
            printf("pos_hysteresis_max=%d", motors_control_get_position_parameter_hysteresis_max());
            println();
            printf("pos_mv_min=%d", motors_control_get_position_parameter_mv_min());
            println();
            printf("pos_mv_max=%d", motors_control_get_position_parameter_mv_max());
            println();
            printf("pos_gain_P=%f", motors_control_get_position_parameter_gain_P());
            println();
            printf("pos_gain_Ti=%f", motors_control_get_position_parameter_gain_Ti());
            println();

            printf("vel_error_min=%d", motors_control_get_velocity_parameter_error_min());
            println();
            printf("vel_error_max=%d", motors_control_get_velocity_parameter_error_max());
            println();
            printf("vel_ignore_min=%d", motors_control_get_velocity_parameter_ignore_min());
            println();
            printf("vel_ignore_max=%d", motors_control_get_velocity_parameter_ignore_max());
            println();
            printf("vel_hysteresis_min=%d", motors_control_get_velocity_parameter_hysteresis_min());
            println();
            printf("vel_hysteresis_max=%d", motors_control_get_velocity_parameter_hysteresis_max());
            println();
            printf("vel_mv_min=%d", motors_control_get_velocity_parameter_mv_min());
            println();
            printf("vel_mv_max=%d", motors_control_get_velocity_parameter_mv_max());
            println();
            printf("vel_gain_P=%f", motors_control_get_velocity_parameter_gain_P());
            println();
            printf("vel_gain_Ti=%f", motors_control_get_velocity_parameter_gain_Ti());
            println();
            break;
        } else if (2 == argc || 3 == argc) {
            char buf[MSCONF_MAX_INPUT_LENGTH];
            msopt_get_argv(msopt, 1, buf, sizeof (buf));
            if (0 == strcmp("pos_error_min", buf)) {
                if (2 == argc) {
                    printf("%d", motors_control_get_position_parameter_error_min());
                } else {
                    msopt_get_argv(msopt, 2, buf, sizeof (buf));
                    motors_control_set_position_parameter_error_min(strtol(buf, NULL, 10));
                    printf("OK");
                }
                break;
            } else if (0 == strcmp("pos_error_max", buf)) {
                if (2 == argc) {
                    printf("%d", motors_control_get_position_parameter_error_max());
                } else {
                    msopt_get_argv(msopt, 2, buf, sizeof (buf));
                    motors_control_set_position_parameter_error_max(strtol(buf, NULL, 10));
                    printf("OK");
                }
                break;
            } else if (0 == strcmp("pos_ignore_min", buf)) {
                if (2 == argc) {
                    printf("%d", motors_control_get_position_parameter_ignore_min());
                } else {
                    msopt_get_argv(msopt, 2, buf, sizeof (buf));
                    motors_control_set_position_parameter_ignore_min(strtol(buf, NULL, 10));
                    printf("OK");
                }
                break;
            } else if (0 == strcmp("pos_ignore_max", buf)) {
                if (2 == argc) {
                    printf("%d", motors_control_get_position_parameter_ignore_max());
                } else {
                    msopt_get_argv(msopt, 2, buf, sizeof (buf));
                    motors_control_set_position_parameter_ignore_max(strtol(buf, NULL, 10));
                    printf("OK");
                }
                break;
            } else if (0 == strcmp("pos_hysteresis_min", buf)) {
                if (2 == argc) {
                    printf("%d", motors_control_get_position_parameter_hysteresis_min());
                } else {
                    msopt_get_argv(msopt, 2, buf, sizeof (buf));
                    motors_control_set_position_parameter_hysteresis_min(strtol(buf, NULL, 10));
                    printf("OK");
                }
                break;
            } else if (0 == strcmp("pos_hysteresis_max", buf)) {
                if (2 == argc) {
                    printf("%d", motors_control_get_position_parameter_hysteresis_max());
                } else {
                    msopt_get_argv(msopt, 2, buf, sizeof (buf));
                    motors_control_set_position_parameter_hysteresis_max(strtol(buf, NULL, 10));
                    printf("OK");
                }
                break;
            } else if (0 == strcmp("pos_mv_min", buf)) {
                if (2 == argc) {
                    printf("%d", motors_control_get_position_parameter_mv_min());
                } else {
                    msopt_get_argv(msopt, 2, buf, sizeof (buf));
                    motors_control_set_position_parameter_mv_min(strtol(buf, NULL, 10));
                    printf("OK");
                }
                break;
            } else if (0 == strcmp("pos_mv_max", buf)) {
                if (2 == argc) {
                    printf("%d", motors_control_get_position_parameter_mv_max());
                } else {
                    msopt_get_argv(msopt, 2, buf, sizeof (buf));
                    motors_control_set_position_parameter_mv_max(strtol(buf, NULL, 10));
                    printf("OK");
                }
                break;
            } else if (0 == strcmp("pos_gain_P", buf)) {
                if (2 == argc) {
                    printf("%f", motors_control_get_position_parameter_gain_P());
                } else {
                    msopt_get_argv(msopt, 2, buf, sizeof (buf));
                    motors_control_set_position_parameter_gain_P(atof(buf));
                    printf("OK");
                }
                break;
            } else if (0 == strcmp("pos_gain_Ti", buf)) {
                if (2 == argc) {
                    printf("%d", motors_control_get_position_parameter_gain_Ti());
                } else {
                    msopt_get_argv(msopt, 2, buf, sizeof (buf));
                    motors_control_set_position_parameter_gain_Ti(atof(buf));
                    printf("OK");
                }
                break;
            }
            
            else if (0 == strcmp("vel_error_min", buf)) {
                if (2 == argc) {
                    printf("%d", motors_control_get_velocity_parameter_error_min());
                } else {
                    msopt_get_argv(msopt, 2, buf, sizeof (buf));
                    motors_control_set_velocity_parameter_error_min(strtol(buf, NULL, 10));
                    printf("OK");
                }
                break;
            } else if (0 == strcmp("vel_error_max", buf)) {
                if (2 == argc) {
                    printf("%d", motors_control_get_velocity_parameter_error_max());
                } else {
                    msopt_get_argv(msopt, 2, buf, sizeof (buf));
                    motors_control_set_velocity_parameter_error_max(strtol(buf, NULL, 10));
                    printf("OK");
                }
                break;
            } else if (0 == strcmp("vel_ignore_min", buf)) {
                if (2 == argc) {
                    printf("%d", motors_control_get_velocity_parameter_ignore_min());
                } else {
                    msopt_get_argv(msopt, 2, buf, sizeof (buf));
                    motors_control_set_velocity_parameter_ignore_min(strtol(buf, NULL, 10));
                    printf("OK");
                }
                break;
            } else if (0 == strcmp("vel_ignore_max", buf)) {
                if (2 == argc) {
                    printf("%d", motors_control_get_velocity_parameter_ignore_max());
                } else {
                    msopt_get_argv(msopt, 2, buf, sizeof (buf));
                    motors_control_set_velocity_parameter_ignore_max(strtol(buf, NULL, 10));
                    printf("OK");
                }
                break;
            } else if (0 == strcmp("vel_hysteresis_min", buf)) {
                if (2 == argc) {
                    printf("%d", motors_control_get_velocity_parameter_hysteresis_min());
                } else {
                    msopt_get_argv(msopt, 2, buf, sizeof (buf));
                    motors_control_set_velocity_parameter_hysteresis_min(strtol(buf, NULL, 10));
                    printf("OK");
                }
                break;
            } else if (0 == strcmp("vel_hysteresis_max", buf)) {
                if (2 == argc) {
                    printf("%d", motors_control_get_velocity_parameter_hysteresis_max());
                } else {
                    msopt_get_argv(msopt, 2, buf, sizeof (buf));
                    motors_control_set_velocity_parameter_hysteresis_max(strtol(buf, NULL, 10));
                    printf("OK");
                }
                break;
            } else if (0 == strcmp("vel_mv_min", buf)) {
                if (2 == argc) {
                    printf("%d", motors_control_get_velocity_parameter_mv_min());
                } else {
                    msopt_get_argv(msopt, 2, buf, sizeof (buf));
                    motors_control_set_velocity_parameter_mv_min(strtol(buf, NULL, 10));
                    printf("OK");
                }
                break;
            } else if (0 == strcmp("vel_mv_max", buf)) {
                if (2 == argc) {
                    printf("%d", motors_control_get_velocity_parameter_mv_max());
                } else {
                    msopt_get_argv(msopt, 2, buf, sizeof (buf));
                    motors_control_set_velocity_parameter_mv_max(strtol(buf, NULL, 10));
                    printf("OK");
                }
                break;
            } else if (0 == strcmp("vel_gain_P", buf)) {
                if (2 == argc) {
                    printf("%f", motors_control_get_velocity_parameter_gain_P());
                } else {
                    msopt_get_argv(msopt, 2, buf, sizeof (buf));
                    motors_control_set_velocity_parameter_gain_P(atof(buf));
                    printf("OK");
                }
                break;
            } else if (0 == strcmp("vel_gain_Ti", buf)) {
                if (2 == argc) {
                    printf("%d", motors_control_get_velocity_parameter_gain_Ti());
                } else {
                    msopt_get_argv(msopt, 2, buf, sizeof (buf));
                    motors_control_set_velocity_parameter_gain_Ti(atof(buf));
                    printf("OK");
                }
                break;
            }
        }
        printf("NG");
    } while (0);
    return 0;
}

static MSCMD_USER_RESULT usrcmd_EEPROM(MSOPT *msopt, MSCMD_USER_OBJECT usrobj) {
    int argc;
    msopt_get_argc(msopt, &argc);
    do {
        if (0) {
        } else if (2 == argc) {
            char buf[MSCONF_MAX_INPUT_LENGTH];
            msopt_get_argv(msopt, 1, buf, sizeof (buf));
            if (0 == strcmp("store", buf)) {
                parameter_store();
                printf("OK");
                break;
            } else if (0 == strcmp("load", buf)) {
                parameter_load();
                printf("OK");
                break;
            } else if (0 == strcmp("default", buf)) {
                parameter_default();
                printf("OK");
                break;
            }
        }
        printf("NG");
    } while (0);
    return 0;
}

static MSCMD_USER_RESULT usrcmd_Position(MSOPT *msopt, MSCMD_USER_OBJECT usrobj) {
    int argc;
    msopt_get_argc(msopt, &argc);
    do {
        if (0) {
        } else if (1 == argc) {
            int i;
            motors_control_feedback();
            for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
                if (i) {
                    printf(" ");
                }
                printf("%d", motors_control_get_actual_position(i));
            }
            break;
        } else if (2 == argc) {
            char buf[MSCONF_MAX_INPUT_LENGTH];
            msopt_get_argv(msopt, 1, buf, sizeof (buf));
            if ('j' == buf[0]) {
                int i;
                printf("{");
                println();
                printf("  \"current_cycle\": %u,", motors_control_feedback());
                println();
                printf("  \"mode\":[");
                for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
                    if (i) {
                        printf(", ");
                    }
                    printf("%02X", motors_control_get_mode(i));
                }
                printf("]");
                println();
                printf("  \"cycle\":[");
                for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
                    if (i) {
                        printf(", ");
                    }
                    printf("%u", motors_control_get_cycle_of_last_command(i));
                }
                printf("]");
                println();
                printf("  \"actual_position\":[");
                for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
                    if (i) {
                        printf(", ");
                    }
                    printf("%d", motors_control_get_actual_position(i));
                }
                printf("]");
                println();
                printf("}");
                break;
            }
        }
        printf("NG");
    } while (0);
    return 0;
}

static MSCMD_USER_RESULT usrcmd_PWM(MSOPT *msopt, MSCMD_USER_OBJECT usrobj) {
    int argc;
    msopt_get_argc(msopt, &argc);
    do {
        if (0) {
        } else if (1 == argc) {
            int i;
            motors_control_feedback();
            for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
                if (i) {
                    printf(" ");
                }
                printf("%d", motors_control_get_demand_pwm(i));
            }
            break;
        } else if (2 == argc) {
            char buf[MSCONF_MAX_INPUT_LENGTH];
            msopt_get_argv(msopt, 1, buf, sizeof (buf));
            if ('j' == buf[0]) {
                int i;
                printf("{");
                println();
                printf("  \"current_cycle\": %u,", motors_control_feedback());
                println();
                printf("  \"mode\":[");
                for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
                    if (i) {
                        printf(", ");
                    }
                    printf("%02X", motors_control_get_mode(i));
                }
                printf("]");
                println();
                printf("  \"cycle\":[");
                for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
                    if (i) {
                        printf(", ");
                    }
                    printf("%u", motors_control_get_cycle_of_last_command(i));
                }
                printf("]");
                println();
                printf("  \"actual_position\":[");
                for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
                    if (i) {
                        printf(", ");
                    }
                    printf("%d", motors_control_get_demand_pwm(i));
                }
                printf("]");
                println();
                printf("}");
                break;
            }
        }
        printf("NG");
    } while (0);
    return 0;
}

static MSCMD_USER_RESULT usrcmd_Echo(MSOPT *msopt, MSCMD_USER_OBJECT usrobj) {
    int argc;
    msopt_get_argc(msopt, &argc);
    do {
        if (0) {
        } else if (1 == argc) {
            printf("%d", shell_echo);
            break;
        } else if (2 == argc) {
            char buf[MSCONF_MAX_INPUT_LENGTH];
            msopt_get_argv(msopt, 1, buf, sizeof (buf));
            if ('0' == buf[0]) {
                shell_echo = 0;
                printf("echo off");
                break;
            } else if ('1' == buf[0]) {
                shell_echo = 1;
                printf("echo on");
                break;
            }
        }
        printf("NG");
    } while (0);
    return 0;
}

#ifdef SUPPORT_I2C
static MSCMD_USER_RESULT usrcmd_BMX055(MSOPT *msopt, MSCMD_USER_OBJECT usrobj) {
    int argc;
    msopt_get_argc(msopt, &argc);
    do {
        if (0) {
        } else if (1 == argc) {
            if (BMX055_found) {
                printf("%d ", BMX055_data.ACC.X);
                printf("%d ", BMX055_data.ACC.Y);
                printf("%d ", BMX055_data.ACC.Z);
                printf("%d ", BMX055_data.GYR.X);
                printf("%d ", BMX055_data.GYR.Y);
                printf("%d ", BMX055_data.GYR.Z);
                printf("%d ", BMX055_data.MAG.X);
                printf("%d ", BMX055_data.MAG.Y);
                printf("%d", BMX055_data.MAG.Z);
            } else {
                printf("NG");
            }
            break;
        } else if (2 == argc) {
            char buf[MSCONF_MAX_INPUT_LENGTH];
            msopt_get_argv(msopt, 1, buf, sizeof (buf));
            if ('j' == buf[0]) {
                printf("{");
                println();
                printf("  \"current_cycle\":  %u,", motors_control_feedback());
                println();
                if (BMX055_found) {
                    printf("  \"BMX055ACC\":[");
                    printf("%d, ", BMX055_data.ACC.X);
                    printf("%d, ", BMX055_data.ACC.Y);
                    printf("%d],", BMX055_data.ACC.Z);
                    println();
                    printf("  \"BMX055GYR\":[");
                    printf("%d, ", BMX055_data.GYR.X);
                    printf("%d, ", BMX055_data.GYR.Y);
                    printf("%d],", BMX055_data.GYR.Z);
                    println();
                    printf("  \"BMX055MAG\":[");
                    printf("%d, ", BMX055_data.MAG.X);
                    printf("%d, ", BMX055_data.MAG.Y);
                    printf("%d]", BMX055_data.MAG.Z);
                    println();
                } else {
                    printf("  \"BMX055ACC\":null,");
                    println();
                    printf("  \"BMX055GYR\":null,");
                    println();
                    printf("  \"BMX055MAG\":null");
                    println();
                }
                printf("}");
                break;
            }
        }
        printf("NG");
    } while (0);
    return 0;
}
#endif

static MSCMD_USER_RESULT usrcmd_SeriConON(MSOPT *msopt, MSCMD_USER_OBJECT usrobj) {
    mode_sericon_or_not_microshell = 1;
    printf("OK");
    return 0;
}

static MSCMD_USER_RESULT usrcmd_Version(MSOPT *msopt, MSCMD_USER_OBJECT usrobj) {
//    printf("Version %d.%02X.%02X", SOFTWARE_VERSION >> 16, 0xFF & (SOFTWARE_VERSION >> 8), 0xFF & SOFTWARE_VERSION);
    printf("Version: %s", SOFTWARE_VERSION_STR);
    return 0;
}

static MSCMD_USER_RESULT usrcmd_Help(MSOPT *msopt, MSCMD_USER_OBJECT usrobj) {
    int argc;
    msopt_get_argc(msopt, &argc);
    do {
        if (0) {
        } else if (1 == argc) {
            printf("************ shell command list ************");
            println();
            printf("cmd    : set new command for motors");
            println();
            printf("exe    : execute new command set");
            println();
            printf("exi    : execute immediately");
            println();
            printf("prm    : set/get parameters");
            println();
            printf("eep    : eeprom access for parameters");
            println();
            printf("pos    : returns current position");
            println();
            printf("pwm    : returns current pwm");
            println();
            printf("i2c    : access i2c devices");
            println();
#ifdef SUPPORT_I2C
            printf("bmx055 : read 9-axes sensor values");
            println();
#endif
            printf("scon   : enter SerCon mode");
            println();
            printf("echo   : shell echo setting");
            println();
            printf("ver    : software version");
            println();
            printf("led    : led indication state and override");
            println();
            printf("?      : this help");
            println();
            printf("********************************************");
            println();
            printf("if you need more information, then type \"? ?\"");
            break;
        } else if (2 == argc) {
            char buf[MSCONF_MAX_INPUT_LENGTH];
            msopt_get_argv(msopt, 1, buf, sizeof (buf));
            if (0) {
            } else if (!strcmp("cmd", buf)) {
                printf("# 3 arguments");
                println();
                printf("cmd [index] [mode] [target]");
                println();
                printf("[index] index of motors (index value range from 0 to %d)", MOTORS_CONTROL_MOTOR_COUNT - 1);
                println();
                printf("[mode] mode of control for motors");
                println();
                printf("%02X : free (target value is ignored)", MOTORS_CONTROL_MODE_NONE);
                println();
                printf("%02X : brake (target value range is from 0 to 1023, otherwise -1023 acts same as 1023)", MOTORS_CONTROL_MODE_BRK);
                println();
                printf("%02X : preset position (target value range is from 0 to 1023, otherwise -1023 acts same as 1023)", MOTORS_CONTROL_MODE_PST);
                println();
                printf("%02X : direct pwm (target value range is from -1023 to 1023)", MOTORS_CONTROL_MODE_PWM);
                println();
                printf("%02X : position mode", MOTORS_CONTROL_MODE_POS);
                println();
                printf("[target] target value of control for motors");
                break;
            } else if (!strcmp("exe", buf)) {
                printf("# 0 arguments");
                println();
                printf("exe");
                println();
                printf("previous commands (cmd) will be executed by this command");
                break;
            } else if (!strcmp("exi", buf)) {
                printf("# 3 arguments");
                println();
                printf("exi [mode]");
                int i;
                for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
                    printf(" [target%d]", i);
                }
                println();
                printf("[mode] mode of control for motors");
                println();
                printf("%02X : free (target value is ignored)", MOTORS_CONTROL_MODE_NONE);
                println();
                printf("%02X : brake (target value range is from 0 to 1023, otherwise -1023 acts same as 1023)", MOTORS_CONTROL_MODE_BRK);
                println();
                printf("%02X : preset position (target value range is from 0 to 1023, otherwise -1023 acts same as 1023)", MOTORS_CONTROL_MODE_PST);
                println();
                printf("%02X : direct pwm (target value range is from -1023 to 1023)", MOTORS_CONTROL_MODE_PWM);
                println();
                printf("%02X : position mode", MOTORS_CONTROL_MODE_POS);
                println();
                for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
                    printf("[target%d] target value of %d%s motor", i, i + 1, (0 == i && (i < 10 || i >= 20)) ? "st" : (1 == i && (i < 10 || i >= 20)) ? "nd" : (2 == i && (i < 10 || i >= 20)) ? "rd" : "th");
                    println();
                }
                printf("execute immediately");
                break;
            } else if (!strcmp("prm", buf)) {
                printf("# 0 arguments");
                println();
                printf("list all parameters");
                println();
                printf("# 2 arguments");
                println();
                printf("prm [name]");
                println();
                printf("get value [value] of parameter [name]");
                println();
                printf("# 3 arguments");
                println();
                printf("set value [value] to parameter [name]");
                break;
            } else if (!strcmp("eep", buf)) {
                printf("# 1 arguments");
                println();
                printf("eep [store|load|default]");
                println();
                printf("eeprom access for parameters");
                println();
                printf("store : store current values to eeprom");
                println();
                printf("load : load eeprom to working set");
                println();
                printf("default : load default values to working set (no eeprom update)");
                break;
            } else if (!strcmp("pos", buf)) {
                printf("# 0 arguments");
                println();
                printf("pos");
                println();
                printf("returns all motors' actual position");
                println();
                printf("1 arguments");
                println();
                printf("pos j");
                println();
                printf("returns all motors' actual position as json format");
                break;
            } else if (!strcmp("pwm", buf)) {
                printf("# 0 arguments");
                println();
                printf("pwm");
                println();
                printf("returns all motors' actual pwm value");
                println();
                printf("1 arguments");
                println();
                printf("pwm j");
                println();
                printf("returns all motors' actual pwm value as json format");
                break;
#ifdef SUPPORT_I2C
            } else if (!strcmp("bmx055", buf)) {
                printf("# 0 arguments");
                println();
                printf("bmx055");
                println();
                printf("returns BMX055's current values");
                println();
                printf("# 1 arguments");
                println();
                printf("pwm j");
                println();
                printf("returns BMX055's current values as json format");
                break;
#endif
            } else if (!strcmp("scon", buf)) {
                printf("# 0 arguments");
                println();
                printf("scon");
                println();
                printf("enter SeriCon mode");
                break;
            } else if (!strcmp("echo", buf)) {
                printf("# 0 arguments");
                println();
                printf("echo");
                println();
                printf("returns current shell's echo status");
                println();
                printf("# 1 arguments");
                println();
                printf("echo [0|1]");
                println();
                printf("set shell's echo ON (1) / OFF (0)");
                break;
            } else if (!strcmp("ver", buf)) {
                printf("# 0 arguments");
                println();
                printf("ver");
                println();
                printf("returns software version");
                println();
            } else if (!strcmp("led", buf)) {
                printf("# 0 arguments");
                println();
                printf("led");
                println();
                printf("returns current led status");
                println();
                printf("# 1 arguments");
                println();
                printf("led [0|1]");
                println();
                printf("set led ON (1) / OFF (0)");
                break;
            } else if (!strcmp("?", buf)) {
                printf("# 0 arguments");
                println();
                printf("?");
                println();
                printf("returns shell command list");
                println();
                printf("# 1 arguments");
                println();
                printf("? [shell_command]");
                println();
                printf("tells detail of shell_command");
                break;
            }
        }
        printf("NG");
    } while (0);
    return 0;
}

static const MSCMD_COMMAND_TABLE command_table[] = {
    { "cmd", usrcmd_Command},
    { "exe", usrcmd_Execute},
    { "exi", usrcmd_ExecuteImmediate},
    { "prm", usrcmd_Parameter},
    { "eep", usrcmd_EEPROM},
    { "pos", usrcmd_Position},
    { "pwm", usrcmd_PWM},
#ifdef SUPPORT_I2C
    { "bmx055", usrcmd_BMX055},
#endif
    { "scon", usrcmd_SeriConON},
    { "echo", usrcmd_Echo},
    { "ver", usrcmd_Version},
    { "led", usrcmd_LED},
    { "?", usrcmd_Help}
};

void communication_init(void) {
    serial_init();
    SeriCon_init(&sericon, SERICON_RX_BUFFER_SIZE, sericon_rx_buffer, SERICON_TX_BUFFER_SIZE, sericon_tx_buffer, sericon_handler, &sericon_hal);
    memset(microshell_buffer, 0, sizeof (microshell_buffer));
    microshell_buffer_idx = 0;
    microshell_init(&microshell, microshell_tx, microshell_rx, microshell_action_hook);
    mscmd_init(&mscmd, (MSCMD_COMMAND_TABLE *) command_table, sizeof (command_table) / sizeof (MSCMD_COMMAND_TABLE), NULL);
    println();
    println();
    printf("%d motors controller", MOTORS_CONTROL_MOTOR_COUNT);
    println();
    printf("by (c) 2020 Shoji Yamamoto");
    println();
    printf("Build: " __DATE__ " " __TIME__);
    println();
//    printf("Version: %d.%02X.%02X", SOFTWARE_VERSION >> 16, 0xFF & (SOFTWARE_VERSION >> 8), 0xFF & SOFTWARE_VERSION);
    printf("Version: %s", SOFTWARE_VERSION_STR);
    println();
    parameter_load();
}

static void _communication_task(void) {
    if (mode_sericon_or_not_microshell) {
        SeriCon_task(&sericon);
    } else if (NULL == SeriCon_getTxBuffer(&sericon)) {
        SeriCon_task(&sericon);
    } else if (serial_available()) {
        static char return_code = 0;
        int rx = serial_get();
        if (EOF == rx) {
            return;
        }
        if ((rx != '\r') && (rx != '\n') && (microshell_buffer_idx < MSCONF_MAX_INPUT_LENGTH)) {
            if (shell_echo) {
                serial_put((uint8_t) rx);
            }
            microshell_buffer[microshell_buffer_idx] = (char) rx;
            microshell_buffer_idx++;
        } else if (microshell_buffer_idx) {
            if (shell_echo && !return_code) {
                println();
            }
            MSCMD_USER_RESULT result;
            if (mscmd_execute(&mscmd, microshell_buffer, &result)) {
                printf("NG");
            }
            println();
            if (shell_echo) {
                serial_puts(":");
            }
            memset(microshell_buffer, 0, sizeof (microshell_buffer));
            microshell_buffer_idx = 0;
        }
        return_code = (rx == '\r') || (rx == '\n');
    }
}

static uint8_t enable_yield = 0;

void I2C_MASTER_yield(void) {
    if (enable_yield) {
        _communication_task();
    }
}

void communication_task(void) {
#ifdef SUPPORT_I2C
    static BMX055_Raw raw;
    if (BMX055_found) {
        BMX055_convert(&BMX055_data, &raw);
        BMX055_found = !BMX055_read(&raw);
    } else {
        BMX055_init();
        BMX055_found = !BMX055_read(&raw);
    }
#endif
    _communication_task();
    enable_yield = 1;
}