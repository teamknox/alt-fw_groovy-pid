/**
 * @file      servo.h
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-PID (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _SERVO_H_
#define	_SERVO_H_

#include <xc.h>
#include <stdint.h>

typedef struct S_ServoParameter
{
  int8_t FixedPointSize;
  int8_t UniDirectional;
  int8_t Reserved[2];
  int16_t ErrorMin;
  int16_t ErrorMax;
  int16_t IgnoreMin;
  int16_t IgnoreMax;
  int32_t MV_HysteresisMin;
  int32_t MV_HysteresisMax;
  int32_t MV_Min;
  int32_t MV_Max;
  int32_t Kp;
  int32_t Ki;
} ServoParameter;

typedef struct S_ServoWorking
{
    int16_t Error;
    int32_t MV;
    const ServoParameter* Parameter;
} ServoWorking;


#include <xc.h>
#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    extern void servo_init(ServoWorking* obj);
    extern inline int16_t servo_task(ServoWorking* obj, int16_t target, int16_t actual);

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* _SERVO_H_ */

