# 1 "SeriCon_lib/SeriCon.c"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 288 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/Applications/microchip/mplabx/v5.40/packs/Microchip/PIC18F-K_DFP/1.4.87/xc8/pic/include/language_support.h" 1 3
# 2 "<built-in>" 2
# 1 "SeriCon_lib/SeriCon.c" 2
# 19 "SeriCon_lib/SeriCon.c"
# 1 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdint.h" 1 3



# 1 "/Applications/microchip/xc8/v2.20/pic/include/c99/musl_xc8.h" 1 3
# 5 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdint.h" 2 3
# 22 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdint.h" 3
# 1 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 1 3
# 127 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef unsigned long uintptr_t;
# 142 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long intptr_t;
# 158 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef signed char int8_t;




typedef short int16_t;




typedef __int24 int24_t;




typedef long int32_t;





typedef long long int64_t;
# 188 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long long intmax_t;





typedef unsigned char uint8_t;




typedef unsigned short uint16_t;




typedef __uint24 uint24_t;




typedef unsigned long uint32_t;





typedef unsigned long long uint64_t;
# 229 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef unsigned long long uintmax_t;
# 23 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdint.h" 2 3

typedef int8_t int_fast8_t;

typedef int64_t int_fast64_t;


typedef int8_t int_least8_t;
typedef int16_t int_least16_t;

typedef int24_t int_least24_t;

typedef int32_t int_least32_t;

typedef int64_t int_least64_t;


typedef uint8_t uint_fast8_t;

typedef uint64_t uint_fast64_t;


typedef uint8_t uint_least8_t;
typedef uint16_t uint_least16_t;

typedef uint24_t uint_least24_t;

typedef uint32_t uint_least32_t;

typedef uint64_t uint_least64_t;
# 139 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdint.h" 3
# 1 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/stdint.h" 1 3
typedef int32_t int_fast16_t;
typedef int32_t int_fast32_t;
typedef uint32_t uint_fast16_t;
typedef uint32_t uint_fast32_t;
# 140 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdint.h" 2 3
# 20 "SeriCon_lib/SeriCon.c" 2
# 1 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdio.h" 1 3
# 10 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdio.h" 3
# 1 "/Applications/microchip/xc8/v2.20/pic/include/c99/features.h" 1 3
# 11 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdio.h" 2 3
# 24 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdio.h" 3
# 1 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 1 3





typedef void * va_list[1];




typedef void * __isoc_va_list[1];
# 122 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef unsigned size_t;
# 137 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long ssize_t;
# 246 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long long off_t;
# 399 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef struct _IO_FILE FILE;
# 25 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdio.h" 2 3
# 52 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdio.h" 3
typedef union _G_fpos64_t {
 char __opaque[16];
 double __align;
} fpos_t;

extern FILE *const stdin;
extern FILE *const stdout;
extern FILE *const stderr;





FILE *fopen(const char *restrict, const char *restrict);
FILE *freopen(const char *restrict, const char *restrict, FILE *restrict);
int fclose(FILE *);

int remove(const char *);
int rename(const char *, const char *);

int feof(FILE *);
int ferror(FILE *);
int fflush(FILE *);
void clearerr(FILE *);

int fseek(FILE *, long, int);
long ftell(FILE *);
void rewind(FILE *);

int fgetpos(FILE *restrict, fpos_t *restrict);
int fsetpos(FILE *, const fpos_t *);

size_t fread(void *restrict, size_t, size_t, FILE *restrict);
size_t fwrite(const void *restrict, size_t, size_t, FILE *restrict);

int fgetc(FILE *);
int getc(FILE *);
int getchar(void);
int ungetc(int, FILE *);

int fputc(int, FILE *);
int putc(int, FILE *);
int putchar(int);

char *fgets(char *restrict, int, FILE *restrict);

char *gets(char *);


int fputs(const char *restrict, FILE *restrict);
int puts(const char *);

#pragma printf_check(printf) const
#pragma printf_check(vprintf) const
#pragma printf_check(sprintf) const
#pragma printf_check(snprintf) const
#pragma printf_check(vsprintf) const
#pragma printf_check(vsnprintf) const

int printf(const char *restrict, ...);
int fprintf(FILE *restrict, const char *restrict, ...);
int sprintf(char *restrict, const char *restrict, ...);
int snprintf(char *restrict, size_t, const char *restrict, ...);

int vprintf(const char *restrict, __isoc_va_list);
int vfprintf(FILE *restrict, const char *restrict, __isoc_va_list);
int vsprintf(char *restrict, const char *restrict, __isoc_va_list);
int vsnprintf(char *restrict, size_t, const char *restrict, __isoc_va_list);

int scanf(const char *restrict, ...);
int fscanf(FILE *restrict, const char *restrict, ...);
int sscanf(const char *restrict, const char *restrict, ...);
int vscanf(const char *restrict, __isoc_va_list);
int vfscanf(FILE *restrict, const char *restrict, __isoc_va_list);
int vsscanf(const char *restrict, const char *restrict, __isoc_va_list);

void perror(const char *);

int setvbuf(FILE *restrict, char *restrict, int, size_t);
void setbuf(FILE *restrict, char *restrict);

char *tmpnam(char *);
FILE *tmpfile(void);




FILE *fmemopen(void *restrict, size_t, const char *restrict);
FILE *open_memstream(char **, size_t *);
FILE *fdopen(int, const char *);
FILE *popen(const char *, const char *);
int pclose(FILE *);
int fileno(FILE *);
int fseeko(FILE *, off_t, int);
off_t ftello(FILE *);
int dprintf(int, const char *restrict, ...);
int vdprintf(int, const char *restrict, __isoc_va_list);
void flockfile(FILE *);
int ftrylockfile(FILE *);
void funlockfile(FILE *);
int getc_unlocked(FILE *);
int getchar_unlocked(void);
int putc_unlocked(int, FILE *);
int putchar_unlocked(int);
ssize_t getdelim(char **restrict, size_t *restrict, int, FILE *restrict);
ssize_t getline(char **restrict, size_t *restrict, FILE *restrict);
int renameat(int, const char *, int, const char *);
char *ctermid(char *);







char *tempnam(const char *, const char *);
# 21 "SeriCon_lib/SeriCon.c" 2
# 1 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdlib.h" 1 3
# 21 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdlib.h" 3
# 1 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 1 3
# 18 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long int wchar_t;
# 22 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdlib.h" 2 3

int atoi (const char *);
long atol (const char *);
long long atoll (const char *);
double atof (const char *);

float strtof (const char *restrict, char **restrict);
double strtod (const char *restrict, char **restrict);
long double strtold (const char *restrict, char **restrict);



long strtol (const char *restrict, char **restrict, int);
unsigned long strtoul (const char *restrict, char **restrict, int);
long long strtoll (const char *restrict, char **restrict, int);
unsigned long long strtoull (const char *restrict, char **restrict, int);

int rand (void);
void srand (unsigned);

          void abort (void);
int atexit (void (*) (void));
          void exit (int);
          void _Exit (int);

void *bsearch (const void *, const void *, size_t, size_t, int (*)(const void *, const void *));

__attribute__((nonreentrant)) void qsort (void *, size_t, size_t, int (*)(const void *, const void *));

int abs (int);
long labs (long);
long long llabs (long long);

typedef struct { int quot, rem; } div_t;
typedef struct { long quot, rem; } ldiv_t;
typedef struct { long long quot, rem; } lldiv_t;

div_t div (int, int);
ldiv_t ldiv (long, long);
lldiv_t lldiv (long long, long long);

typedef struct { unsigned int quot, rem; } udiv_t;
typedef struct { unsigned long quot, rem; } uldiv_t;
udiv_t udiv (unsigned int, unsigned int);
uldiv_t uldiv (unsigned long, unsigned long);





size_t __ctype_get_mb_cur_max(void);
# 22 "SeriCon_lib/SeriCon.c" 2
# 1 "/Applications/microchip/xc8/v2.20/pic/include/c99/string.h" 1 3
# 25 "/Applications/microchip/xc8/v2.20/pic/include/c99/string.h" 3
# 1 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 1 3
# 411 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef struct __locale_struct * locale_t;
# 26 "/Applications/microchip/xc8/v2.20/pic/include/c99/string.h" 2 3

void *memcpy (void *restrict, const void *restrict, size_t);
void *memmove (void *, const void *, size_t);
void *memset (void *, int, size_t);
int memcmp (const void *, const void *, size_t);
void *memchr (const void *, int, size_t);

char *strcpy (char *restrict, const char *restrict);
char *strncpy (char *restrict, const char *restrict, size_t);

char *strcat (char *restrict, const char *restrict);
char *strncat (char *restrict, const char *restrict, size_t);

int strcmp (const char *, const char *);
int strncmp (const char *, const char *, size_t);

int strcoll (const char *, const char *);
size_t strxfrm (char *restrict, const char *restrict, size_t);

char *strchr (const char *, int);
char *strrchr (const char *, int);

size_t strcspn (const char *, const char *);
size_t strspn (const char *, const char *);
char *strpbrk (const char *, const char *);
char *strstr (const char *, const char *);
char *strtok (char *restrict, const char *restrict);

size_t strlen (const char *);

char *strerror (int);
# 65 "/Applications/microchip/xc8/v2.20/pic/include/c99/string.h" 3
char *strtok_r (char *restrict, const char *restrict, char **restrict);
int strerror_r (int, char *, size_t);
char *stpcpy(char *restrict, const char *restrict);
char *stpncpy(char *restrict, const char *restrict, size_t);
size_t strnlen (const char *, size_t);
char *strdup (const char *);
char *strndup (const char *, size_t);
char *strsignal(int);
char *strerror_l (int, locale_t);
int strcoll_l (const char *, const char *, locale_t);
size_t strxfrm_l (char *restrict, const char *restrict, size_t, locale_t);




void *memccpy (void *restrict, const void *restrict, int, size_t);
# 23 "SeriCon_lib/SeriCon.c" 2
# 1 "SeriCon_lib/SeriCon.h" 1
# 29 "SeriCon_lib/SeriCon.h"
    typedef struct S_SeriCon SeriCon;
    typedef struct S_SeriConHAL SeriConHAL;
    typedef int(SeriConRx) (void);
    typedef int(SeriConTx) (uint8_t data);
    typedef uint16_t (SeriConTimer) (void);
    struct S_SeriConHAL {
        SeriConTimer *Timer;
        SeriConRx *Rx;
        SeriConTx *Tx;
    };

    typedef int (SeriConHandler) (SeriCon *sc, size_t sz, const uint8_t *rx);

    struct S_SeriCon {
        int RxIndex;
        int TxIndex;
        int RxState;
        int RxSize;
        int TxSize;
        int RxWorkSize;
        int TxWorkSize;
        uint8_t *RxWorkBuffer;
        uint8_t *TxWorkBuffer;
        uint16_t RxCRC;
        uint16_t RxTime;
        uint16_t RxTimeout;
        uint16_t RX_TIMEOUT;
        uint16_t RxErrorCount;
        SeriConHandler *Handler;
        const SeriConHAL *HAL;
        uint8_t TxID;
        uint8_t RxID;
    };

    extern void SeriCon_init(SeriCon *sc,
            size_t rxWorkSize, uint8_t *rxWorkBuffer,
            size_t txWorkSize, uint8_t *txWorkBuffer,
            SeriConHandler *handler, const SeriConHAL *hal);
    extern int SeriCon_send(SeriCon *sc, size_t txSize, const uint8_t *txBuffer);
    uint8_t *SeriCon_getTxBuffer(SeriCon *sc);
    uint16_t SeriCon_getRxErrorCount(SeriCon *sc);
    uint16_t SeriCon_getRxTimeout(SeriCon *sc);
    void SeriCon_setRxTimeout(SeriCon *sc, uint16_t time);
    void SeriCon_task(SeriCon *sc);
# 24 "SeriCon_lib/SeriCon.c" 2
# 1 "SeriCon_lib/CRC16_CCITT.h" 1



# 1 "SeriCon_lib/crc_lib.h" 1
# 84 "SeriCon_lib/crc_lib.h"
typedef struct { uint8_t Table[256]; uint8_t Initial;} uint8_t_SET;
typedef struct { uint16_t Table[256]; uint16_t Initial;} uint16_t_SET;
typedef struct { uint32_t Table[256]; uint32_t Initial;} uint32_t_SET;

extern void uint8_t_InitializeSET(uint8_t_SET* set, uint8_t ipolynominal, uint8_t initial);extern uint8_t uint8_t_CRC(const uint8_t_SET* set, const void* data, uint8_t crc);extern uint8_t uint8_t_ContinueCRC(const uint8_t_SET* set, const void* data, size_t size, uint8_t crc);extern uint8_t uint8_t_GetCRC(const uint8_t_SET* set, const void* data, size_t size);
extern void uint16_t_InitializeSET(uint16_t_SET* set, uint16_t ipolynominal, uint16_t initial);extern uint16_t uint16_t_CRC(const uint16_t_SET* set, const void* data, uint16_t crc);extern uint16_t uint16_t_ContinueCRC(const uint16_t_SET* set, const void* data, size_t size, uint16_t crc);extern uint16_t uint16_t_GetCRC(const uint16_t_SET* set, const void* data, size_t size);
extern void uint32_t_InitializeSET(uint32_t_SET* set, uint32_t ipolynominal, uint32_t initial);extern uint32_t uint32_t_CRC(const uint32_t_SET* set, const void* data, uint32_t crc);extern uint32_t uint32_t_ContinueCRC(const uint32_t_SET* set, const void* data, size_t size, uint32_t crc);extern uint32_t uint32_t_GetCRC(const uint32_t_SET* set, const void* data, size_t size);
# 5 "SeriCon_lib/CRC16_CCITT.h" 2
# 18 "SeriCon_lib/CRC16_CCITT.h"
extern const uint16_t_SET CRC16_CCITT
# 54 "SeriCon_lib/CRC16_CCITT.h"
;
# 25 "SeriCon_lib/SeriCon.c" 2

void SeriCon_init(SeriCon *sc,
        size_t rxWorkSize, uint8_t *rxWorkBuffer, size_t txWorkSize, uint8_t *txWorkBuffer,
        SeriConHandler *handler, const SeriConHAL *hal) {
    sc->RxCRC = CRC16_CCITT.Initial;
    sc->RxSize = 0;
    sc->TxIndex = 0;
    sc->RxIndex = 0;
    sc->RxTime = 0;
    sc->RxTimeout = 0;
    sc->TxSize = 0;
    sc->RxState = 0;
    sc->RxErrorCount = 0;
    sc->RxID = 0;
    sc->TxID = 0;
    sc->RX_TIMEOUT = 5UL;
    sc->RxWorkSize = (int) rxWorkSize;
    sc->TxWorkSize = (int) txWorkSize;
    sc->RxWorkBuffer = rxWorkBuffer;
    sc->TxWorkBuffer = txWorkBuffer;
    sc->Handler = handler;
    sc->HAL = hal;
}

uint8_t *SeriCon_getTxBuffer(SeriCon *sc) {
    uint8_t* ptr = (sc->TxSize) ? ((void*)0) : &sc->TxWorkBuffer[4];
    return ptr;
}

int SeriCon_send(SeriCon *sc, size_t txSize, const uint8_t *txBuffer) {
    uint16_t txCRC;
    if (sc->TxSize || (txSize > 256) || ((int) (txSize + 5) > sc->TxWorkSize) || (0 == txSize)) {
        return 0;
    }
    sc->TxID++;
    sc->TxWorkBuffer[0] = 0xFF;
    sc->TxWorkBuffer[1] = 0x55;
    sc->TxWorkBuffer[2] = sc->TxID;
    sc->TxWorkBuffer[3] = (uint8_t) (txSize - 1);
    if (&sc->TxWorkBuffer[4] != txBuffer) {
        memcpy(&sc->TxWorkBuffer[4], txBuffer, txSize);
    }
    txCRC = uint16_t_GetCRC(&CRC16_CCITT, sc->TxWorkBuffer, txSize + 4);
    sc->TxWorkBuffer[txSize + 4] = (uint8_t) (txCRC);
    sc->TxWorkBuffer[txSize + 5] = (uint8_t) (txCRC >> 8);
    sc->TxIndex = 0;
    sc->TxSize = (int) (txSize + 6);
    return (int) txSize;
}

uint16_t SeriCon_getRxErrorCount(SeriCon *sc) {
    return sc->RxErrorCount;
}

uint16_t SeriCon_getRxTimeout(SeriCon *sc) {
    return sc->RX_TIMEOUT;
}

void SeriCon_setRxTimeout(SeriCon *sc, uint16_t time) {
    sc->RX_TIMEOUT = time;
}

void SeriCon_task(SeriCon *sc) {
    while (sc->TxSize) {
        if (sc->HAL->Tx(sc->TxWorkBuffer[sc->TxIndex])) {
            break;
        }
        sc->TxSize--;
        sc->TxIndex++;
    }
    if (sc->RxState > 0 && 0 == sc->TxSize) {
        if (sc->Handler(sc, sc->RxWorkBuffer[3] + 1, &sc->RxWorkBuffer[4])) {
            sc->RxState = 0;
            sc->RxIndex = 0;
            sc->RxSize = 0;
        }
    }
    int continue_rx = 1;
    while (continue_rx) {
        if (0 == sc->RxIndex || sc->RxTimeout) {
            int c = (-1);
            uint16_t now = sc->HAL->Timer();
            if (sc->RxState <= 0) {
                c = sc->HAL->Rx();
            }
            if (c != (-1)) {
                sc->RxTimeout = sc->RX_TIMEOUT;
                if (sc->RxIndex >= sc->RxWorkSize) {
                    sc->RxState = -1;
                } else if (0 == sc->RxState) {
                    if (0 == sc->RxIndex) {
                        sc->RxCRC = CRC16_CCITT.Initial;
                    }
                    sc->RxWorkBuffer[sc->RxIndex] = (uint8_t) c;
                    sc->RxCRC = uint16_t_CRC(&CRC16_CCITT, sc->RxWorkBuffer[sc->RxIndex], sc->RxCRC);
                    if (0 == sc->RxIndex) {
                        if (0xFF != c) {
                            sc->RxState = -1;
                        }
                    } else if (1 == sc->RxIndex) {
                        if (0x55 != c) {
                            sc->RxState = -1;
                        }
                    } else if (2 == sc->RxIndex) {
                    } else if (3 == sc->RxIndex) {
                        sc->RxSize = (int) c;
                        sc->RxSize += 7;
                        if (sc->RxSize > sc->RxWorkSize) {
                            sc->RxState = -1;
                        }
                    } else if (4 <= sc->RxIndex && sc->RxIndex <= (sc->RxSize - 2)) {
                    } else if (sc->RxIndex == (sc->RxSize - 1)) {
                        if (sc->RxCRC) {
                            sc->RxState = -1;
                        } else {
                            if (sc->RxID == sc->RxWorkBuffer[2]) {
                                sc->RxState = -1;
                            } else {
                                sc->RxState = 1;
                            }
                            sc->RxID = sc->RxWorkBuffer[2];
                        }
                    }
                }
                sc->RxIndex++;
            } else {
                uint16_t diff;
                if (now >= sc->RxTime) {
                    diff = now - sc->RxTime;
                } else {
                    diff = 0xFFFFU - sc->RxTime;
                    diff += now + 1;
                }
                if (diff >= sc->RxTimeout) {
                    if (sc->RxSize && sc->RxIndex != sc->RxSize) {
                        sc->RxState = -1;
                    }
                    if (sc->RxState < 0) {
                        sc->RxState = 0;
                        sc->RxErrorCount++;
                    }
                    sc->RxTimeout = 0;
                    sc->RxIndex = 0;
                    sc->RxSize = 0;
                } else {
                    sc->RxTimeout -= diff;
                }
                continue_rx = 0;
            }
            sc->RxTime = now;
        }
    }
}
