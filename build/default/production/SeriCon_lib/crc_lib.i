# 1 "SeriCon_lib/crc_lib.c"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 288 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/Applications/microchip/mplabx/v5.40/packs/Microchip/PIC18F-K_DFP/1.4.87/xc8/pic/include/language_support.h" 1 3
# 2 "<built-in>" 2
# 1 "SeriCon_lib/crc_lib.c" 2
# 23 "SeriCon_lib/crc_lib.c"
# 1 "SeriCon_lib/crc_lib.h" 1
# 29 "SeriCon_lib/crc_lib.h"
# 1 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdio.h" 1 3



# 1 "/Applications/microchip/xc8/v2.20/pic/include/c99/musl_xc8.h" 1 3
# 5 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdio.h" 2 3





# 1 "/Applications/microchip/xc8/v2.20/pic/include/c99/features.h" 1 3
# 11 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdio.h" 2 3
# 24 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdio.h" 3
# 1 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 1 3





typedef void * va_list[1];




typedef void * __isoc_va_list[1];
# 122 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef unsigned size_t;
# 137 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long ssize_t;
# 168 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef __int24 int24_t;
# 204 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef __uint24 uint24_t;
# 246 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long long off_t;
# 399 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef struct _IO_FILE FILE;
# 25 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdio.h" 2 3
# 52 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdio.h" 3
typedef union _G_fpos64_t {
 char __opaque[16];
 double __align;
} fpos_t;

extern FILE *const stdin;
extern FILE *const stdout;
extern FILE *const stderr;





FILE *fopen(const char *restrict, const char *restrict);
FILE *freopen(const char *restrict, const char *restrict, FILE *restrict);
int fclose(FILE *);

int remove(const char *);
int rename(const char *, const char *);

int feof(FILE *);
int ferror(FILE *);
int fflush(FILE *);
void clearerr(FILE *);

int fseek(FILE *, long, int);
long ftell(FILE *);
void rewind(FILE *);

int fgetpos(FILE *restrict, fpos_t *restrict);
int fsetpos(FILE *, const fpos_t *);

size_t fread(void *restrict, size_t, size_t, FILE *restrict);
size_t fwrite(const void *restrict, size_t, size_t, FILE *restrict);

int fgetc(FILE *);
int getc(FILE *);
int getchar(void);
int ungetc(int, FILE *);

int fputc(int, FILE *);
int putc(int, FILE *);
int putchar(int);

char *fgets(char *restrict, int, FILE *restrict);

char *gets(char *);


int fputs(const char *restrict, FILE *restrict);
int puts(const char *);

#pragma printf_check(printf) const
#pragma printf_check(vprintf) const
#pragma printf_check(sprintf) const
#pragma printf_check(snprintf) const
#pragma printf_check(vsprintf) const
#pragma printf_check(vsnprintf) const

int printf(const char *restrict, ...);
int fprintf(FILE *restrict, const char *restrict, ...);
int sprintf(char *restrict, const char *restrict, ...);
int snprintf(char *restrict, size_t, const char *restrict, ...);

int vprintf(const char *restrict, __isoc_va_list);
int vfprintf(FILE *restrict, const char *restrict, __isoc_va_list);
int vsprintf(char *restrict, const char *restrict, __isoc_va_list);
int vsnprintf(char *restrict, size_t, const char *restrict, __isoc_va_list);

int scanf(const char *restrict, ...);
int fscanf(FILE *restrict, const char *restrict, ...);
int sscanf(const char *restrict, const char *restrict, ...);
int vscanf(const char *restrict, __isoc_va_list);
int vfscanf(FILE *restrict, const char *restrict, __isoc_va_list);
int vsscanf(const char *restrict, const char *restrict, __isoc_va_list);

void perror(const char *);

int setvbuf(FILE *restrict, char *restrict, int, size_t);
void setbuf(FILE *restrict, char *restrict);

char *tmpnam(char *);
FILE *tmpfile(void);




FILE *fmemopen(void *restrict, size_t, const char *restrict);
FILE *open_memstream(char **, size_t *);
FILE *fdopen(int, const char *);
FILE *popen(const char *, const char *);
int pclose(FILE *);
int fileno(FILE *);
int fseeko(FILE *, off_t, int);
off_t ftello(FILE *);
int dprintf(int, const char *restrict, ...);
int vdprintf(int, const char *restrict, __isoc_va_list);
void flockfile(FILE *);
int ftrylockfile(FILE *);
void funlockfile(FILE *);
int getc_unlocked(FILE *);
int getchar_unlocked(void);
int putc_unlocked(int, FILE *);
int putchar_unlocked(int);
ssize_t getdelim(char **restrict, size_t *restrict, int, FILE *restrict);
ssize_t getline(char **restrict, size_t *restrict, FILE *restrict);
int renameat(int, const char *, int, const char *);
char *ctermid(char *);







char *tempnam(const char *, const char *);
# 30 "SeriCon_lib/crc_lib.h" 2
# 1 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdint.h" 1 3
# 22 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdint.h" 3
# 1 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 1 3
# 127 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef unsigned long uintptr_t;
# 142 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long intptr_t;
# 158 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef signed char int8_t;




typedef short int16_t;
# 173 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long int32_t;





typedef long long int64_t;
# 188 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long long intmax_t;





typedef unsigned char uint8_t;




typedef unsigned short uint16_t;
# 209 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef unsigned long uint32_t;





typedef unsigned long long uint64_t;
# 229 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef unsigned long long uintmax_t;
# 23 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdint.h" 2 3

typedef int8_t int_fast8_t;

typedef int64_t int_fast64_t;


typedef int8_t int_least8_t;
typedef int16_t int_least16_t;

typedef int24_t int_least24_t;

typedef int32_t int_least32_t;

typedef int64_t int_least64_t;


typedef uint8_t uint_fast8_t;

typedef uint64_t uint_fast64_t;


typedef uint8_t uint_least8_t;
typedef uint16_t uint_least16_t;

typedef uint24_t uint_least24_t;

typedef uint32_t uint_least32_t;

typedef uint64_t uint_least64_t;
# 139 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdint.h" 3
# 1 "/Applications/microchip/xc8/v2.20/pic/include/c99/bits/stdint.h" 1 3
typedef int32_t int_fast16_t;
typedef int32_t int_fast32_t;
typedef uint32_t uint_fast16_t;
typedef uint32_t uint_fast32_t;
# 140 "/Applications/microchip/xc8/v2.20/pic/include/c99/stdint.h" 2 3
# 31 "SeriCon_lib/crc_lib.h" 2
# 84 "SeriCon_lib/crc_lib.h"
typedef struct { uint8_t Table[256]; uint8_t Initial;} uint8_t_SET;
typedef struct { uint16_t Table[256]; uint16_t Initial;} uint16_t_SET;
typedef struct { uint32_t Table[256]; uint32_t Initial;} uint32_t_SET;

static void uint8_t_GenerateTable(uint8_t table[256], uint8_t ipolynominal) { int dividend; uint8_t remainder; uint8_t bit; for (dividend = 0; dividend < 256; dividend++) { remainder = dividend; for (bit = 8; bit > 0; bit--) { if (remainder & 1) { remainder = (remainder >> 1) ^ ipolynominal; } else { remainder = (remainder >> 1); } } table[dividend] = remainder; }}void uint8_t_InitializeSET(uint8_t_SET* set, uint8_t ipolynominal, uint8_t initial) { uint8_t_GenerateTable(set->Table, ipolynominal); set->Initial = initial;}uint8_t uint8_t_CRC(const uint8_t_SET* set, const void* data, uint8_t crc) { const uint8_t *p = (const uint8_t*) data; return crc = set->Table[crc ^ *p];}uint8_t uint8_t_ContinueCRC(const uint8_t_SET* set, const void* data, size_t size, uint8_t crc) { const uint8_t *p = (const uint8_t*) data; while (size) { crc = set->Table[crc ^ *p]; p++; size--; } return crc;}uint8_t uint8_t_GetCRC(const uint8_t_SET* set, const void* data, size_t size) { return uint8_t_ContinueCRC(set, data, size, set->Initial);}
static void uint16_t_GenerateTable(uint16_t table[256], uint16_t ipolynominal) { int dividend; uint16_t remainder; uint8_t bit; for (dividend = 0; dividend < 256; dividend++) { remainder = dividend; for (bit = 8; bit > 0; bit--) { if (remainder & 1) { remainder = (remainder >> 1) ^ ipolynominal; } else { remainder = (remainder >> 1); } } table[dividend] = remainder; }}void uint16_t_InitializeSET(uint16_t_SET* set, uint16_t ipolynominal, uint16_t initial) { uint16_t_GenerateTable(set->Table, ipolynominal); set->Initial = initial;}uint16_t uint16_t_CRC(const uint16_t_SET* set, const void* data, uint16_t crc) { const uint8_t *p = (const uint8_t*) data; return crc = set->Table[crc ^ *p];}uint16_t uint16_t_ContinueCRC(const uint16_t_SET* set, const void* data, size_t size, uint16_t crc) { const uint8_t *p = (const uint8_t*) data; while (size) { crc = set->Table[crc ^ *p]; p++; size--; } return crc;}uint16_t uint16_t_GetCRC(const uint16_t_SET* set, const void* data, size_t size) { return uint16_t_ContinueCRC(set, data, size, set->Initial);}
static void uint32_t_GenerateTable(uint32_t table[256], uint32_t ipolynominal) { int dividend; uint32_t remainder; uint8_t bit; for (dividend = 0; dividend < 256; dividend++) { remainder = dividend; for (bit = 8; bit > 0; bit--) { if (remainder & 1) { remainder = (remainder >> 1) ^ ipolynominal; } else { remainder = (remainder >> 1); } } table[dividend] = remainder; }}void uint32_t_InitializeSET(uint32_t_SET* set, uint32_t ipolynominal, uint32_t initial) { uint32_t_GenerateTable(set->Table, ipolynominal); set->Initial = initial;}uint32_t uint32_t_CRC(const uint32_t_SET* set, const void* data, uint32_t crc) { const uint8_t *p = (const uint8_t*) data; return crc = set->Table[crc ^ *p];}uint32_t uint32_t_ContinueCRC(const uint32_t_SET* set, const void* data, size_t size, uint32_t crc) { const uint8_t *p = (const uint8_t*) data; while (size) { crc = set->Table[crc ^ *p]; p++; size--; } return crc;}uint32_t uint32_t_GetCRC(const uint32_t_SET* set, const void* data, size_t size) { return uint32_t_ContinueCRC(set, data, size, set->Initial);}
# 24 "SeriCon_lib/crc_lib.c" 2

