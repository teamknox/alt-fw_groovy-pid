/**
 * @file      servo.c
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-PID (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <xc.h>
#include <stdint.h>
#include "servo.h"
#include "led.h"

void servo_init(ServoWorking* obj)
{
    obj->Error = 0;
    obj->MV = 0;
}

int16_t inline servo_task(ServoWorking* obj, int16_t target, int16_t actual)
{
  int16_t error = target - actual;
  if (error > obj->Parameter->ErrorMax) error = obj->Parameter->ErrorMax;
  else if (error < obj->Parameter->ErrorMin) error = obj->Parameter->ErrorMin;
  if ((obj->Parameter->IgnoreMin <= error) && (error <= obj->Parameter->IgnoreMax)) {
    obj->Error = 0;
    obj->MV = 0;
  } else {
    if (error > 0 && obj->MV < obj->Parameter->MV_HysteresisMax)
    {
       obj->MV = obj->Parameter->MV_HysteresisMax;
    }
    else if(error < 0 && obj->MV > obj->Parameter->MV_HysteresisMin)
    {
       obj->MV = obj->Parameter->MV_HysteresisMin;
    }
    else
    {
        int32_t dMV_p = obj->Parameter->Kp * (int32_t)(error - obj->Error);
        int32_t dMV_i = obj->Parameter->Ki * (int32_t)error;
        int32_t dMV = dMV_p;
        dMV += dMV_i;
        if (dMV_p > 0 && dMV_i > 0 && dMV < 0) {
          dMV = obj->Parameter->MV_Max;
        } else if (dMV_p < 0 && dMV_i < 0 && dMV > 0) {
          dMV = obj->Parameter->MV_Min;
        } else {
          if (dMV  > obj->Parameter->MV_Max) {
            dMV = obj->Parameter->MV_Max;
          } else if (obj->MV  < obj->Parameter->MV_Min) {
            dMV  = obj->Parameter->MV_Min;
          }
        }
        int32_t _MV = obj->MV + dMV;
        if (dMV > 0 && obj->MV > 0 && _MV < 0) {
          obj->MV  = obj->Parameter->MV_Max;
        } else if (dMV < 0 && obj->MV < 0 && _MV > 0) {
          obj->MV  = obj->Parameter->MV_Min;
        } else {
          obj->MV = _MV;
          if (obj->MV  > obj->Parameter->MV_Max) {
            obj->MV  = obj->Parameter->MV_Max;
          } else if (obj->MV  < obj->Parameter->MV_Min) {
            obj->MV  = obj->Parameter->MV_Min;
          }
        }
        obj->Error = error;
    }
  }
  if (obj->Parameter->UniDirectional) {
    if (obj->MV > 0 && target < 0) {
      return -(int16_t)(1 << obj->Parameter->FixedPointSize);
    } else if (obj->MV < 0 && target > 0) {
      return (int16_t)(1 << obj->Parameter->FixedPointSize);
    } else {
      return (int16_t)(obj->MV >> obj->Parameter->FixedPointSize);
    }
  } else {
    return (int16_t)(obj->MV >> obj->Parameter->FixedPointSize);
  }
}