/**
 * @file      main.c
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-PID (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include "sys_clock.h"
#include <xc.h>
#include <stdio.h>
#include "led.h"
#include "motors_control.h"
#include "serial.h"
#include "encoder.h"
#include "i2c_master.h"
#include "communication.h"
#include "config/mcu_config.h"

void __interrupt(low_priority) ISR_LOW(void) {
    motors_control_isr();
    serial_low_isr();
}

void __interrupt(high_priority) ISR_HIGH(void) {
    encoder_isr();
    serial_high_isr();
}

void main(void) {
    sys_clock_init();
    IPEN = 1;
    led_init();
#ifdef SUPPORT_I2C
    I2C_MASTER_init();
#endif
    motors_control_init();
    communication_init();
    GIEH = 1;
    GIEL = 1;
    led_on();
    while (1) {
        communication_task();
    }
    return;
}
