/**
 * @file      i2c_master.c
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-PID (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include "sys_clock.h"
#include <xc.h>
#include <stdint.h>

#include "i2c_master.h"
#include "config/i2c_master_config.h"

/** I2Cバス状態フラグ */
static uint8_t I2C_MASTER_Flags;

#define I2C_MASTER_FLAG_BUS_DIRTY 0x1
#define I2C_MASTER_Flags_set_bus_dirty()	do{I2C_MASTER_Flags |=  I2C_MASTER_FLAG_BUS_DIRTY;}while(0)
#define I2C_MASTER_Flags_clear_bus_dirty()	do{I2C_MASTER_Flags &= ~I2C_MASTER_FLAG_BUS_DIRTY;}while(0)
#define I2C_MASTER_Flags_is_bus_dirty()	(I2C_MASTER_Flags & I2C_MASTER_FLAG_BUS_DIRTY)

/**
 * I2Cバスリセット
 * 
 * バスラインがスレーブデバイスによってロックされているときにリセットし、バスを開放する。 
 */
static I2C_MASTER_RET I2C_MASTER_bus_reset(void) {
    int i;
    I2C_MASTER_RET ret;
    /* SCL=Hi、SDA=Hiを出力 */
    __delay_us(5);
    if (0 == I2C_SCL_PORT) {
        /* SCLがLOWにロックされている（ハードウェア不良） */
        return I2C_MASTER_ERR_SCL_LOW;
    }
    /* SDAがHiに戻ってくるまでクロックを出力する */
    for (i = 0; i < 10; i++) {
        if (1 == I2C_SDA_PORT) {
            break;
        }
        I2C_SCL_TRIS = 0;
        __delay_us(5);
        I2C_SCL_TRIS = 1;
        __delay_us(5);
    }
    (void) i;
    if (0 == I2C_SDA_PORT) {
        /* 10回試行で解放されない（スレーブ異常もしくはハードウェア不良） */
        ret = I2C_MASTER_ERR_SDA_LOW;
    } else if (0 == I2C_SCL_PORT) {
        /* SCLがLOWにロックされている（ハードウェア不良） */
        ret = I2C_MASTER_ERR_SCL_LOW;
    } else {
        ret = I2C_MASTER_RET_OK;
    }
    I2C_SDA_TRIS = 0;
    __delay_us(5);
    I2C_SDA_TRIS = 1;
    __delay_us(5);
    /* 正常にバスをリセットした */
    return ret;
}

/** 
 * スタートコンディション 
 */
static I2C_MASTER_RET I2C_MASTER_start(void) {
    uint16_t t;
    /* スタートコンディション */
    SSP_SEN = 1;
    Nop();
    if (SSP_WCOL) {
        SSP_SEN = 0;
        SSP_WCOL = 0;
        return I2C_MASTER_ERR_WCOL;
    }
    /* スタートコンディション処理完了待ち */
    t = (uint16_t) (_XTAL_FREQ / FSCL);
    while (SSP_SEN) {
        if (t) {
            t--;
        } else {
            return I2C_MASTER_ERR_TIMEOUT_HW;
        }
        I2C_MASTER_yield();
    }
    return I2C_MASTER_RET_OK;
}

/**
 * ストップコンディション
 */
static I2C_MASTER_RET I2C_MASTER_stop(void) {
    uint16_t t;
    SSP_IF = 0;
    /* ストップコンディション */
    SSP_PEN = 1;
    Nop();
    /* ストップコンディション処理完了待ち */
    t = (uint16_t) (_XTAL_FREQ / FSCL);
    while (SSP_PEN) {
        if (t) {
            t--;
        } else {
            return I2C_MASTER_ERR_TIMEOUT_HW;
        }
        I2C_MASTER_yield();
    }
    SSP_IF = 0;
    return I2C_MASTER_RET_OK;
}

/**
 * リスタートコンディション
 */
static I2C_MASTER_RET I2C_MASTER_restart(void) {
    uint16_t t;
    SSP_IF = 0;
    /* リスタートコンディション */
    SSP_RSEN = 1;
    Nop();
    /* リスタートコンディション処理完了待ち */
    t = (uint16_t) (_XTAL_FREQ / FSCL);
    while (SSP_RSEN) {
        if (t) {
            t--;
        } else {
            return I2C_MASTER_ERR_TIMEOUT_HW;
        }
        I2C_MASTER_yield();
    }
    return I2C_MASTER_RET_OK;
}

I2C_MASTER_RET I2C_MASTER_recover_bus(void) {
    SSP_SSPEN = 0;
    if (I2C_MASTER_RET_OK != I2C_MASTER_bus_reset()) {
        /* ハードウェア不良（PCB等を要確認） */
        return I2C_MASTER_ERR_HARDWARE;
    }
    SSP_SSPEN = 1;
    return I2C_MASTER_RET_OK;
}

/**
 * 1バイト送信
 * @param data 送信データ
 */
static I2C_MASTER_RET I2C_MASTER_write_byte(uint8_t data) {
    uint16_t t;
    if (SSP_BF) {
        /* すでに送信処理中 */
        return I2C_MASTER_ERR_BF;
    }
    SSP_IF = 0;
    /* 送信要求 */
    SSP_BUF = data;
    /* 送信完了待ち */
    t = (uint16_t) (8 * (_XTAL_FREQ / FSCL));
    while (!SSP_IF) {
        if (t) {
            t--;
        } else {
            return I2C_MASTER_ERR_SCL_LOW;
        }
        I2C_MASTER_yield();
    }
    /* ACK確認 */
    if (SSP_ACKSTAT) {
        /* NACK */
        return I2C_MASTER_ERR_NAK;
    }
    /* 正常終了 */
    return I2C_MASTER_RET_OK;
}

/**
 * ペリフェラルのエラークリア
 */
static void I2C_MASTER_clear_errors(void) {
    SSP_RCEN = 0;
    SSP_WCOL = 0;
}

I2C_MASTER_RET I2C_MASTER_poll(uint8_t dev_addr) {
    uint8_t slave_addr = (dev_addr << 1) | 0;
    I2C_MASTER_RET retval;
    if (I2C_MASTER_Flags_is_bus_dirty()) {
        I2C_MASTER_clear_errors();
        if (I2C_MASTER_RET_OK != I2C_MASTER_recover_bus()) {
            return I2C_MASTER_ERR_HARDWARE;
        }
        I2C_MASTER_Flags_clear_bus_dirty();
    }
    if (I2C_MASTER_RET_OK != I2C_MASTER_start()) {
        I2C_MASTER_Flags_set_bus_dirty();
        return I2C_MASTER_ERR_COMM_FAIL;
    }
    retval = I2C_MASTER_write_byte(slave_addr);
    (void) slave_addr;
    if (I2C_MASTER_ERR_NAK == retval) {
        retval = I2C_MASTER_ERR_BAD_ADDR;
    } else if (I2C_MASTER_RET_OK != retval) {
        retval = I2C_MASTER_ERR_COMM_FAIL;
    } else {
        retval = I2C_MASTER_RET_OK;
    }
    if (I2C_MASTER_RET_OK != I2C_MASTER_stop()) {
        I2C_MASTER_Flags_set_bus_dirty();
        return I2C_MASTER_ERR_COMM_FAIL;
    }
    return retval;
}

void I2C_MASTER_init(void) {
    /* バスをマニュアルでリセットできるようにGPIOを初期化しておく */
    I2C_SETUP_ANSEL();
    I2C_SCL_TRIS = 1;
    I2C_SDA_TRIS = 1;
    I2C_SCL_LAT = 0;
    I2C_SDA_LAT = 0;
    if (!((1 == I2C_SCL_PORT) && (1 == I2C_SDA_PORT))) {
        /* バスロックを検出したのでバスリセットを試みる */
        I2C_MASTER_bus_reset();
    }
    I2C_MASTER_Flags = 0;
    I2C_MASTER_Flags_set_bus_dirty();
    /* ペリフェラル初期化 */
    SSPREG_STAT = 0x80;
    SSPREG_ADD = (_XTAL_FREQ - (4ULL * FSCL)) / (4ULL * FSCL);
    SSPREG_CON1 = 0x28;
    SSPREG_CON2 = 0x00;
    SSP_IF = 0;
}

/**
 * 1バイト読み取り
 * 
 * @param nack_flag 0以外でNACK送信
 * @return 戻り値が負ならエラー、それ以外は正常に受信したデータの値
 */
static int I2C_MASTER_read_byte(uint8_t nack_flag) {
    uint16_t t;
    /* NACK */
    SSP_ACKDT = nack_flag;
    /* 受信開始 */
    SSP_RCEN = 1;
    /* 受信完了待ち */
    t = (uint16_t) (16 * (_XTAL_FREQ / FSCL));
    while (!SSP_BF) {
        if (t) {
            t--;
        } else {
            return I2C_MASTER_ERR_RCV_TIMEOUT;
        }
        I2C_MASTER_yield();
    }
    /* ACK送信 */
    SSP_ACKEN = 1;
    /* ACK送信完了待ち */
    t = (uint16_t) (_XTAL_FREQ / FSCL);
    while (SSP_ACKEN) //HW cleared when complete
    {
        if (t) {
            t--;
        } else {
            return I2C_MASTER_ERR_SCL_LOW;
        }
        I2C_MASTER_yield();
    }
    if (SSP_SSPOV) {
        SSP_SSPOV = 0;
        return I2C_MASTER_ERR_OVER_FLOW;
    }
    return SSP_BUF;
}

I2C_MASTER_RET I2C_MASTER_write(uint8_t dev_addr, uint8_t sub_addr, int size, const uint8_t* src) {
    int i;
    int retval;
    uint8_t slave_addr = (dev_addr << 1) | 0;
    if (I2C_MASTER_Flags_is_bus_dirty()) {
        return I2C_MASTER_ERR_BUS_DIRTY;
    }
    if (I2C_MASTER_RET_OK != I2C_MASTER_start()) //Start
    {
        I2C_MASTER_Flags_set_bus_dirty();
        return I2C_MASTER_ERR_COMM_FAIL;
    }
    if (I2C_MASTER_RET_OK != (retval = I2C_MASTER_write_byte(slave_addr))) {
        I2C_MASTER_stop();
        I2C_MASTER_Flags_set_bus_dirty();
        return (I2C_MASTER_ERR_NAK == (I2C_MASTER_RET) retval) ? I2C_MASTER_ERR_BAD_ADDR : I2C_MASTER_ERR_COMM_FAIL;
    }
    (void) slave_addr;
    if (I2C_MASTER_RET_OK != I2C_MASTER_write_byte(sub_addr)) //Sub Addr
    {
        I2C_MASTER_stop();
        I2C_MASTER_Flags_set_bus_dirty();
        return I2C_MASTER_ERR_COMM_FAIL;
    }
    for (i = 0; i < size; i++) {
        if (I2C_MASTER_RET_OK != I2C_MASTER_write_byte(src[i])) {
            I2C_MASTER_stop();
            I2C_MASTER_Flags_set_bus_dirty();
            return I2C_MASTER_ERR_COMM_FAIL;
        }
        I2C_MASTER_yield();
    }
    if (I2C_MASTER_RET_OK != I2C_MASTER_stop()) {
        I2C_MASTER_Flags_set_bus_dirty();
        return I2C_MASTER_ERR_COMM_FAIL;
    }
    return I2C_MASTER_RET_OK;
}

I2C_MASTER_RET I2C_MASTER_read(uint8_t dev_addr, uint8_t sub_addr, int size, uint8_t* dest) {
    uint8_t slave_addr;
    int retval;
    int i;
    if (I2C_MASTER_Flags_is_bus_dirty()) {
        return I2C_MASTER_ERR_BUS_DIRTY;
    }
    if (I2C_MASTER_RET_OK != I2C_MASTER_start()) //Start
    {
        I2C_MASTER_Flags_set_bus_dirty();
        return I2C_MASTER_ERR_COMM_FAIL;
    }
    slave_addr = (dev_addr << 1) | 0;
    if (I2C_MASTER_RET_OK != (retval = I2C_MASTER_write_byte(slave_addr))) {
        I2C_MASTER_stop();
        I2C_MASTER_Flags_set_bus_dirty();
        return (I2C_MASTER_ERR_NAK == (I2C_MASTER_RET) retval) ? I2C_MASTER_ERR_BAD_ADDR : I2C_MASTER_ERR_COMM_FAIL;
    }
    if (I2C_MASTER_RET_OK != I2C_MASTER_write_byte(sub_addr)) {
        I2C_MASTER_stop();
        I2C_MASTER_Flags_set_bus_dirty();
        return I2C_MASTER_ERR_COMM_FAIL;
    }
    if (I2C_MASTER_RET_OK != I2C_MASTER_restart()) {
        I2C_MASTER_stop();
        I2C_MASTER_Flags_set_bus_dirty();
        return I2C_MASTER_ERR_COMM_FAIL;
    }
    slave_addr = (dev_addr << 1) | 1;
    if (I2C_MASTER_RET_OK != I2C_MASTER_write_byte(slave_addr)) {
        I2C_MASTER_stop();
        I2C_MASTER_Flags_set_bus_dirty();
        return I2C_MASTER_ERR_COMM_FAIL;
    }
    for (i = 0; i < size; i++) {
        retval = I2C_MASTER_read_byte((size - 1) == i);
        if (retval >= 0) {
            dest[i] = (uint8_t) retval;
        } else {
            I2C_MASTER_stop();
            I2C_MASTER_Flags_set_bus_dirty();
            return I2C_MASTER_ERR_COMM_FAIL;
        }
        I2C_MASTER_yield();
    }
    if (I2C_MASTER_RET_OK != I2C_MASTER_stop()) {
        I2C_MASTER_Flags_set_bus_dirty();
        return I2C_MASTER_ERR_COMM_FAIL;
    }
    return I2C_MASTER_RET_OK; //Success
}