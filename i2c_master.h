/**
 * @file      i2c_master.h
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-PID (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _I2C_MASTER_H_
#define _I2C_MASTER_H_

#include <stdint.h>

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

typedef enum E_I2C_MASTER_RET 
{
    /** 正常終了。 */
    I2C_MASTER_RET_OK = 0,
    /** I2Cバスエラーの原因がハードウェア依存（PCBや実装不良を要調査）。 */
    I2C_MASTER_ERR_HARDWARE = -1,
    /** SCLがLOWでロックされたエラー。PCB上のハードウェアもしくはスレーブデバイスがSCLをロックしている。 */
    I2C_MASTER_ERR_SCL_LOW = -2,
    /** SDAがLOWでロックされたエラー */
    I2C_MASTER_ERR_SDA_LOW = -3,
    /** WCOLフラグがマスタ動作中に立った。I2Cペリフェラルのリセットが必要。 */
    I2C_MASTER_ERR_WCOL = -4,
    /** スレーブがデータ送信でNAKを返した */
    I2C_MASTER_ERR_NAK = -5,
    /** BFフラグが立った（ソフトウェア実装バグ） */
    I2C_MASTER_ERR_BF = -6,
    /** 先行処理がおわっていないのに新たに受信した */
    I2C_MASTER_ERR_OVER_FLOW = -7,
    /** データ受信待ちタイムアウト */
    I2C_MASTER_ERR_RCV_TIMEOUT = -8,
    /** I2Cバスリカバーが必要な状態 */
    I2C_MASTER_ERR_BUS_DIRTY = -100,
    /** 原因不明のタイムアウト */
    I2C_MASTER_ERR_TIMEOUT_HW = -101,
    /** 通信失敗 */
    I2C_MASTER_ERR_COMM_FAIL = -102,
    /** 存在しないアドレス、もしくは指定アドレスのデバイス応答しない */
    I2C_MASTER_ERR_BAD_ADDR = -103
} I2C_MASTER_RET;

/**
 * I2Cバスマスター初期化 
 */
extern void I2C_MASTER_init(void);
/**
 * I2CバスマスターYield
 */
extern void I2C_MASTER_yield(void);
/**
 * バスエラーから復帰
 * 
 * @retval I2C_MASTER_ERR_HARDWARE エラーがハードウェア不良による疑いが強いので実装問題がないか確認する。
 * @retval I2C_MASTER_RET_OK 正常にI2Cバスがエラーから回復した。
 */
extern I2C_MASTER_RET I2C_MASTER_recover_bus(void);
/**
 * デバイスの存在確認
 * @param dev_addr デバイスアドレス
 * 
 * @retval I2C_MASTER_ERR_HARDWARE エラーがハードウェア不良による疑いが強いので実装問題がないか確認する。
 * @retval I2C_MASTER_ERR_COMM_FAIL 通信失敗。
 * @retval I2C_MASTER_ERR_BAD_ADDR 存在しないデバイスのアドレスを指定。
 * @retval I2C_MASTER_RET_OK 指定アドレスのデバイスから応答があった。
 */
extern I2C_MASTER_RET I2C_MASTER_poll(uint8_t dev_addr);
/**
 * データ書き込み
 * 
 * @param dev_addr デバイスアドレス
 * @param sub_addr デバイス内の書き込み先頭アドレス
 * @param size 書き込みデータバイトサイズ
 * @param src 書き込みデータ元
 *
 * @retval I2C_MASTER_ERR_BUS_DIRTY バスエラーになった。エラー回復が必要。
 * @retval I2C_MASTER_ERR_COMM_FAIL 通信失敗。エラー回復が必要。
 * @retval I2C_MASTER_ERR_BAD_ADDR 存在しないデバイスのアドレスを指定。
 * @retval I2C_MASTER_RET_OK 正常データを書き込んだ。
 */
extern I2C_MASTER_RET I2C_MASTER_write(uint8_t dev_addr, uint8_t sub_addr, int size, const uint8_t* src);
/**
 * データ読み取り
 * 
 * @param dev_addr デバイスアドレス
 * @param sub_addr デバイス内の読み取り先頭アドレス
 * @param size 読み取りデータバイトサイズ
 * @param dest 読み取りデータの格納先
 * 
 * @retval I2C_MASTER_ERR_BUS_DIRTY バスエラーになった。エラー回復が必要。
 * @retval I2C_MASTER_ERR_COMM_FAIL 通信失敗。エラー回復が必要。
 * @retval I2C_MASTER_ERR_BAD_ADDR 存在しないデバイスのアドレスを指定。
 * @retval I2C_MASTER_RET_OK 正常データを読み取った。
 */
extern I2C_MASTER_RET I2C_MASTER_read(uint8_t dev_addr, uint8_t sub_addr, int size, uint8_t* dest);

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* _I2C_MASTER_H_ */

