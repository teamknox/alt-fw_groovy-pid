/**
 * @file      eeprom_parameter.c
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-PID (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdint.h>
#include <xc.h>
#include "eeprom.h"
#include "parameter.h"
#include "motors_control.h"
#include "SeriCon_lib/CRC16_CCITT.h"

static uint16_t getCRC16(size_t size, uint8_t* data, uint16_t crc16) {
    return uint16_t_ContinueCRC(&CRC16_CCITT, data, size, crc16);
}

void parameter_store(void) {
    uint16_t crc16 = PARAMETER_CRC16_INIT_VALUE;
    uint8_t tmp[4];
    int16_t *p_int16 = (int16_t *) tmp;
    float* p_float = (float*) tmp;
    *p_int16 = motors_control_get_position_parameter_error_min();
    crc16 = getCRC16(2, tmp, crc16);
    eeprom_write_bytes(PARAMETER_POS_ERROR_MIN_EEPROM_ADDRESS, 2, tmp);
    *p_int16 = motors_control_get_position_parameter_error_max();
    crc16 = getCRC16(2, tmp, crc16);
    eeprom_write_bytes(PARAMETER_POS_ERROR_MAX_EEPROM_ADDRESS, 2, tmp);
    *p_int16 = motors_control_get_position_parameter_ignore_min();
    crc16 = getCRC16(2, tmp, crc16);
    eeprom_write_bytes(PARAMETER_POS_IGNORE_MIN_EEPROM_ADDRESS, 2, tmp);
    *p_int16 = motors_control_get_position_parameter_ignore_max();
    crc16 = getCRC16(2, tmp, crc16);
    eeprom_write_bytes(PARAMETER_POS_IGNORE_MAX_EEPROM_ADDRESS, 2, tmp);
    *p_int16 = motors_control_get_position_parameter_hysteresis_min();
    crc16 = getCRC16(2, tmp, crc16);
    eeprom_write_bytes(PARAMETER_POS_HYSTERESIS_MIN_EEPROM_ADDRESS, 2, tmp);
    *p_int16 = motors_control_get_position_parameter_hysteresis_max();
    crc16 = getCRC16(2, tmp, crc16);
    eeprom_write_bytes(PARAMETER_POS_HYSTERESIS_MAX_EEPROM_ADDRESS, 2, tmp);
    *p_int16 = motors_control_get_position_parameter_mv_min();
    crc16 = getCRC16(2, tmp, crc16);
    eeprom_write_bytes(PARAMETER_POS_MV_MIN_EEPROM_ADDRESS, 2, tmp);
    *p_int16 = motors_control_get_position_parameter_mv_max();
    crc16 = getCRC16(2, tmp, crc16);
    eeprom_write_bytes(PARAMETER_POS_MV_MAX_EEPROM_ADDRESS, 2, tmp);
    *p_float = motors_control_get_position_parameter_gain_P();
    crc16 = getCRC16(4, tmp, crc16);
    eeprom_write_bytes(PARAMETER_POS_GAIN_P_EEPROM_ADDRESS, 4, tmp);
    *p_float = motors_control_get_position_parameter_gain_Ti();
    crc16 = getCRC16(4, tmp, crc16);
    eeprom_write_bytes(PARAMETER_POS_GAIN_Ti_EEPROM_ADDRESS, 4, tmp);
    
    *p_int16 = motors_control_get_velocity_parameter_error_min();
    crc16 = getCRC16(2, tmp, crc16);
    eeprom_write_bytes(PARAMETER_VEL_ERROR_MIN_EEPROM_ADDRESS, 2, tmp);
    *p_int16 = motors_control_get_velocity_parameter_error_max();
    crc16 = getCRC16(2, tmp, crc16);
    eeprom_write_bytes(PARAMETER_VEL_ERROR_MAX_EEPROM_ADDRESS, 2, tmp);
    *p_int16 = motors_control_get_velocity_parameter_ignore_min();
    crc16 = getCRC16(2, tmp, crc16);
    eeprom_write_bytes(PARAMETER_VEL_IGNORE_MIN_EEPROM_ADDRESS, 2, tmp);
    *p_int16 = motors_control_get_velocity_parameter_ignore_max();
    crc16 = getCRC16(2, tmp, crc16);
    eeprom_write_bytes(PARAMETER_VEL_IGNORE_MAX_EEPROM_ADDRESS, 2, tmp);
    *p_int16 = motors_control_get_velocity_parameter_hysteresis_min();
    crc16 = getCRC16(2, tmp, crc16);
    eeprom_write_bytes(PARAMETER_VEL_HYSTERESIS_MIN_EEPROM_ADDRESS, 2, tmp);
    *p_int16 = motors_control_get_velocity_parameter_hysteresis_max();
    crc16 = getCRC16(2, tmp, crc16);
    eeprom_write_bytes(PARAMETER_VEL_HYSTERESIS_MAX_EEPROM_ADDRESS, 2, tmp);
    *p_int16 = motors_control_get_velocity_parameter_mv_min();
    crc16 = getCRC16(2, tmp, crc16);
    eeprom_write_bytes(PARAMETER_VEL_MV_MIN_EEPROM_ADDRESS, 2, tmp);
    *p_int16 = motors_control_get_velocity_parameter_mv_max();
    crc16 = getCRC16(2, tmp, crc16);
    eeprom_write_bytes(PARAMETER_VEL_MV_MAX_EEPROM_ADDRESS, 2, tmp);
    *p_float = motors_control_get_velocity_parameter_gain_P();
    crc16 = getCRC16(4, tmp, crc16);
    eeprom_write_bytes(PARAMETER_VEL_GAIN_P_EEPROM_ADDRESS, 4, tmp);
    *p_float = motors_control_get_velocity_parameter_gain_Ti();
    crc16 = getCRC16(4, tmp, crc16);
    eeprom_write_bytes(PARAMETER_VEL_GAIN_Ti_EEPROM_ADDRESS, 4, tmp);
    
    eeprom_write_bytes(PARAMETER_CRC16_EEPROM_ADDRESS, 2, &crc16);
}

void parameter_load(void) {
    uint16_t crc16 = PARAMETER_CRC16_INIT_VALUE;
    uint8_t tmp[4];
    int16_t *p_int16 = (int16_t *) tmp;
    float* p_float = (float*) tmp;
    eeprom_read_bytes(PARAMETER_POS_ERROR_MIN_EEPROM_ADDRESS, 2, tmp);
    crc16 = getCRC16(2, tmp, crc16);
    motors_control_set_position_parameter_error_min(*p_int16);
    eeprom_read_bytes(PARAMETER_POS_ERROR_MAX_EEPROM_ADDRESS, 2, tmp);
    crc16 = getCRC16(2, tmp, crc16);
    motors_control_set_position_parameter_error_max(*p_int16);
    eeprom_read_bytes(PARAMETER_POS_IGNORE_MIN_EEPROM_ADDRESS, 2, tmp);
    crc16 = getCRC16(2, tmp, crc16);
    motors_control_set_position_parameter_ignore_min(*p_int16);
    eeprom_read_bytes(PARAMETER_POS_IGNORE_MAX_EEPROM_ADDRESS, 2, tmp);
    crc16 = getCRC16(2, tmp, crc16);
    motors_control_set_position_parameter_ignore_max(*p_int16);
    eeprom_read_bytes(PARAMETER_POS_HYSTERESIS_MIN_EEPROM_ADDRESS, 2, tmp);
    crc16 = getCRC16(2, tmp, crc16);
    motors_control_set_position_parameter_hysteresis_min(*p_int16);
    eeprom_read_bytes(PARAMETER_POS_HYSTERESIS_MAX_EEPROM_ADDRESS, 2, tmp);
    crc16 = getCRC16(2, tmp, crc16);
    motors_control_set_position_parameter_hysteresis_max(*p_int16);
    eeprom_read_bytes(PARAMETER_POS_MV_MIN_EEPROM_ADDRESS, 2, tmp);
    crc16 = getCRC16(2, tmp, crc16);
    motors_control_set_position_parameter_mv_min(*p_int16);
    eeprom_read_bytes(PARAMETER_POS_MV_MAX_EEPROM_ADDRESS, 2, tmp);
    crc16 = getCRC16(2, tmp, crc16);
    motors_control_set_position_parameter_mv_max(*p_int16);
    eeprom_read_bytes(PARAMETER_POS_GAIN_P_EEPROM_ADDRESS, 4, tmp);
    crc16 = getCRC16(4, tmp, crc16);
    motors_control_set_position_parameter_gain_P(*p_float);
    eeprom_read_bytes(PARAMETER_POS_GAIN_Ti_EEPROM_ADDRESS, 4, tmp);
    crc16 = getCRC16(4, tmp, crc16);
    motors_control_set_position_parameter_gain_Ti(*p_float);

    eeprom_read_bytes(PARAMETER_VEL_ERROR_MIN_EEPROM_ADDRESS, 2, tmp);
    crc16 = getCRC16(2, tmp, crc16);
    motors_control_set_velocity_parameter_error_min(*p_int16);
    eeprom_read_bytes(PARAMETER_VEL_ERROR_MAX_EEPROM_ADDRESS, 2, tmp);
    crc16 = getCRC16(2, tmp, crc16);
    motors_control_set_velocity_parameter_error_max(*p_int16);
    eeprom_read_bytes(PARAMETER_VEL_IGNORE_MIN_EEPROM_ADDRESS, 2, tmp);
    crc16 = getCRC16(2, tmp, crc16);
    motors_control_set_velocity_parameter_ignore_min(*p_int16);
    eeprom_read_bytes(PARAMETER_VEL_IGNORE_MAX_EEPROM_ADDRESS, 2, tmp);
    crc16 = getCRC16(2, tmp, crc16);
    motors_control_set_velocity_parameter_ignore_max(*p_int16);
    eeprom_read_bytes(PARAMETER_VEL_HYSTERESIS_MIN_EEPROM_ADDRESS, 2, tmp);
    crc16 = getCRC16(2, tmp, crc16);
    motors_control_set_velocity_parameter_hysteresis_min(*p_int16);
    eeprom_read_bytes(PARAMETER_VEL_HYSTERESIS_MAX_EEPROM_ADDRESS, 2, tmp);
    crc16 = getCRC16(2, tmp, crc16);
    motors_control_set_velocity_parameter_hysteresis_max(*p_int16);
    eeprom_read_bytes(PARAMETER_VEL_MV_MIN_EEPROM_ADDRESS, 2, tmp);
    crc16 = getCRC16(2, tmp, crc16);
    motors_control_set_velocity_parameter_mv_min(*p_int16);
    eeprom_read_bytes(PARAMETER_VEL_MV_MAX_EEPROM_ADDRESS, 2, tmp);
    crc16 = getCRC16(2, tmp, crc16);
    motors_control_set_velocity_parameter_mv_max(*p_int16);
    eeprom_read_bytes(PARAMETER_VEL_GAIN_P_EEPROM_ADDRESS, 4, tmp);
    crc16 = getCRC16(4, tmp, crc16);
    motors_control_set_velocity_parameter_gain_P(*p_float);
    eeprom_read_bytes(PARAMETER_VEL_GAIN_Ti_EEPROM_ADDRESS, 4, tmp);
    crc16 = getCRC16(4, tmp, crc16);
    motors_control_set_velocity_parameter_gain_Ti(*p_float);

    eeprom_read_bytes(PARAMETER_CRC16_EEPROM_ADDRESS, 2, tmp);
    crc16 = getCRC16(2, tmp, crc16);
    if (crc16) {
        parameter_default();
    }
}

void parameter_default(void) {
    motors_control_set_position_parameter_error_min(PARAMETER_POS_ERROR_MIN_DEFAULT_VALUE);
    motors_control_set_position_parameter_error_max(PARAMETER_POS_ERROR_MAX_DEFAULT_VALUE);
    motors_control_set_position_parameter_ignore_min(PARAMETER_POS_IGNORE_MIN_DEFAULT_VALUE);
    motors_control_set_position_parameter_ignore_max(PARAMETER_POS_IGNORE_MAX_DEFAULT_VALUE);
    motors_control_set_position_parameter_hysteresis_min(PARAMETER_POS_HYSTERESIS_MIN_DEFAULT_VALUE);
    motors_control_set_position_parameter_hysteresis_max(PARAMETER_POS_HYSTERESIS_MAX_DEFAULT_VALUE);
    motors_control_set_position_parameter_mv_min(PARAMETER_POS_MV_MIN_DEFAULT_VALUE);
    motors_control_set_position_parameter_mv_max(PARAMETER_POS_MV_MAX_DEFAULT_VALUE);
    motors_control_set_position_parameter_gain_P(PARAMETER_POS_GAIN_P_DEFAULT_VALUE);
    motors_control_set_position_parameter_gain_Ti(PARAMETER_POS_GAIN_Ti_DEFAULT_VALUE);

    motors_control_set_velocity_parameter_error_min(PARAMETER_VEL_ERROR_MIN_DEFAULT_VALUE);
    motors_control_set_velocity_parameter_error_max(PARAMETER_VEL_ERROR_MAX_DEFAULT_VALUE);
    motors_control_set_velocity_parameter_ignore_min(PARAMETER_VEL_IGNORE_MIN_DEFAULT_VALUE);
    motors_control_set_velocity_parameter_ignore_max(PARAMETER_VEL_IGNORE_MAX_DEFAULT_VALUE);
    motors_control_set_velocity_parameter_hysteresis_min(PARAMETER_VEL_HYSTERESIS_MIN_DEFAULT_VALUE);
    motors_control_set_velocity_parameter_hysteresis_max(PARAMETER_VEL_HYSTERESIS_MAX_DEFAULT_VALUE);
    motors_control_set_velocity_parameter_mv_min(PARAMETER_VEL_MV_MIN_DEFAULT_VALUE);
    motors_control_set_velocity_parameter_mv_max(PARAMETER_VEL_MV_MAX_DEFAULT_VALUE);
    motors_control_set_velocity_parameter_gain_P(PARAMETER_VEL_GAIN_P_DEFAULT_VALUE);
    motors_control_set_velocity_parameter_gain_Ti(PARAMETER_VEL_GAIN_Ti_DEFAULT_VALUE);
}