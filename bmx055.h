/**
 * @file      bmx0255.h
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-PID (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _BMX055_H_
#define	_BMX055_H_

#include <stdint.h>
#include "i2c_master.h"
#include "bmx055.h"

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */


typedef struct S_BMX055_Raw
{
    struct {
        uint16_t X;
        uint16_t Y;
        uint16_t Z;
    } ACC;
    struct {
        uint16_t X;
        uint16_t Y;
        uint16_t Z;
    } GYR;
    struct {
        uint16_t X;
        uint16_t Y;
        uint16_t Z;
    } MAG;
} BMX055_Raw;

typedef struct S_BMX055_Data
{
    struct {
        int16_t X;
        int16_t Y;
        int16_t Z;
    } ACC;
    struct {
        int16_t X;
        int16_t Y;
        int16_t Z;
    } GYR;
    struct {
        int16_t X;
        int16_t Y;
        int16_t Z;
    } MAG;
} BMX055_Data;

extern int BMX055_init(void);
extern int BMX0255_read_ACC_raw(uint8_t xyz[6]);
extern int BMX0255_read_GYR_raw(uint8_t xyz[6]);
extern int BMX0255_read_MAG_raw(uint8_t xyz[6]);
extern int BMX055_read(BMX055_Raw* raw);
extern void BMX055_convert(BMX055_Data* dest, const BMX055_Raw* src);


#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* _BMX055_H_ */

