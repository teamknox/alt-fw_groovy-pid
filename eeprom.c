/**
 * @file      eeprom.c
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-PID (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <xc.h>
#include "eeprom.h"

uint8_t eeprom_read_byte(uint8_t address) {
    volatile uint8_t data;
    EEADR = address;
    EECON1 = 0;
    EECON1bits.RD = 1;
    while (EECON1bits.RD) {
        NOP();
    }
    data = EEDATA;
    return data;
}

void eeprom_read_bytes(uint8_t address, size_t size, uint8_t* data) {
    while (size) {
        *data = eeprom_read_byte(address);
        data++;
        address++;
        size--;
    }
}

void eeprom_write_byte(uint8_t address, uint8_t data) {
    EEADR = address;
    EEDATA = data;
    EECON1 = 0;
    EECON1bits.WREN = 1;
    EECON2 = 0x55;
    EECON2 = 0xAA;
    EECON1bits.WR = 1;
    while (EECON1bits.WR) {
        NOP();
    }
}

void eeprom_write_bytes(uint8_t address, size_t size, const uint8_t* data) {
    while (size) {
        eeprom_write_byte(address, *data);
        data++;
        address++;
        size--;
    }
}
