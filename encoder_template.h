/**
 * @file      encoder_template.h
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-PID (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _ENCODER_TEMPLATE_H_
#define	_ENCODER_TEMPLATE_H_

#include "config/encoder_config.h"

#define ENCODER_TEMPLATE_GENERATE(idx) \
static int16_t encoder_##idx; \
int16_t encoder_##idx##_value(void) { \
    ENCODER_##idx##_A_INTIE = 0; \
    int16_t ret = encoder_##idx; \
    ENCODER_##idx##_A_INTIE = 1; \
    return ret; \
} \
void encoder_##idx##_preset(int16_t value) {\
    ENCODER_##idx##_A_INTIE = 0;\
    encoder_##idx = value;\
    ENCODER_##idx##_A_INTIE = 1;\
}

#define ENCODER_TEMPLATE_GENERATE_INIT(idx)\
do {\
    ENCODER_##idx##_A_INTIP(1);\
    ENCODER_##idx##_A_INTIF = 0;\
    ENCODER_##idx##_A_INTIE = 1;\
} while(0)


#define ENCODER_TEMPLATE_GENERATE_ISR(idx, encoder_port)\
do {\
    if (ENCODER_##idx##_A_INTIF) {\
        ENCODER_##idx##_A_INTIF = 0;\
        if (encoder_port & ENCODER_##idx##_B) {\
            encoder_##idx -= ENCODER_##idx##_DIR;\
        } else {\
            encoder_##idx += ENCODER_##idx##_DIR;\
        }\
    }\
} while(0)

#endif	/* _ENCODER_TEMPLATE_H_ */

