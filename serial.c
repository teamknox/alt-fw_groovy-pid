/**
 * @file      serial.c
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-PID (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include "serial.h"
#include "config/serial_config.h"
#include "led.h"

static uint8_t TxBuffer[64];
static uint8_t RxBuffer[64];
static uint8_t TxWrIdx = 0;
static uint8_t TxRdIdx = 0;
static uint8_t TxLen = 0;
static uint8_t RxWrIdx = 0;
static uint8_t RxRdIdx = 0;
static uint8_t RxLen = 0;

static inline void serial_rx_error_handle(void) {
    volatile uint8_t rcsta = SERIAL_RCSTA;
    if (rcsta & 0b00000100) { // FERR
        volatile uint8_t dummy = SERIAL_RCSTA;
        (void) dummy;
    }
    if (rcsta & 0b00000010) { // OERR
        SERIAL_RCSTA = 0b10000000;
        SERIAL_RCSTA = 0b10010000;
        SERIAL_RCIF = 0;
    }
}

void serial_init(void) {
    SERIAL_SETUP_ANSEL();
    SERIAL_TX_TRIS = 0;
    SERIAL_RX_TRIS = 1;
    SERIAL_TXSTA = 0b00100100;
    SERIAL_RCSTA = 0b10010000;
    SERIAL_BAUDCON = 0b00001000;
    SERIAL_SPBRG = 138; // 115200 bps
    serial_rx_error_handle();
    SERIAL_TXIP = 0;
    SERIAL_TXIF = 0;
    SERIAL_RCIP = 1;
    SERIAL_RCIF = 0;
    SERIAL_RCIE = 1;
}

void serial_high_isr(void) {
    if (SERIAL_RCIF) {
        SERIAL_RCIF = 0;
        //led_toggle();
        if (RxLen < sizeof (RxBuffer)) {
            RxBuffer[RxWrIdx] = SERIAL_RCREG;
            RxLen++;
            RxWrIdx++;
            if (RxWrIdx >= sizeof (RxBuffer)) {
                RxWrIdx = 0;
            }
        } else {
            volatile uint8_t dummy = SERIAL_RCREG;
            (void) dummy;
            SERIAL_RCIE = 0;
        }
    }
}

void serial_low_isr(void) {
    if (SERIAL_TXIF) {
        SERIAL_TXIF = 0;
        if (TxLen) {
            SERIAL_TXREG = TxBuffer[TxRdIdx];
            TxRdIdx++;
            TxLen--;
            if (TxRdIdx >= sizeof (TxBuffer)) {
                TxRdIdx = 0;
            }
        } else {
            SERIAL_TXIE = 0;
        }
    }
}

int serial_available(void) {
    SERIAL_RCIE = 0;
    serial_rx_error_handle();
    SERIAL_RCIE = 1;
    return RxLen;
}

int serial_get(void) {
    int ret = EOF;
    SERIAL_RCIE = 0;
    serial_rx_error_handle();
    if (RxLen) {
        ret = RxBuffer[RxRdIdx];
        RxLen--;
        RxRdIdx++;
        if (RxRdIdx >= sizeof (RxBuffer)) {
            RxRdIdx = 0;
        }
    }
    SERIAL_RCIE = 1;
    return ret;
}

int serial_put(uint8_t data) {
    int ret = EOF;
    if (GIEL) {
        SERIAL_TXIE = 0;
        if (TxLen < sizeof (TxBuffer)) {
            TxBuffer[TxWrIdx] = data;
            TxWrIdx++;
            TxLen++;
            if (TxWrIdx >= sizeof (TxBuffer)) {
                TxWrIdx = 0;
            }
            ret = 0;
        }
        SERIAL_TXIE = 1;
    } else {
        SERIAL_TXIE = 0;
        if (TxLen >= sizeof (TxBuffer)) {
            while (0 == SERIAL_TXIF) {
                Nop();
            }
        }
        if (SERIAL_TXIF) {
            if (TxLen) {
                SERIAL_TXREG = TxBuffer[TxRdIdx];
                TxRdIdx++;
                TxLen--;
                if (TxRdIdx >= sizeof (TxBuffer)) {
                    TxRdIdx = 0;
                }
            } else {
                SERIAL_TXREG = data;
            }
        }
        if (TxLen < sizeof (TxBuffer)) {
            TxBuffer[TxWrIdx] = data;
            TxWrIdx++;
            TxLen++;
            if (TxWrIdx >= sizeof (TxBuffer)) {
                TxWrIdx = 0;
            }
            ret = 0;
        }
        SERIAL_TXIE = 1;
    }
    return ret;
}

int serial_puts(char* str) {
    while (*str) {
        if (serial_put((uint8_t) * str)) {
            return EOF;
        }
        str++;
    }
    return 0;
}

void putch(char data) {
    while (EOF == serial_put((uint8_t) data)) {
        Nop();
    }
}