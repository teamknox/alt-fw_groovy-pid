/**
 * @file      motors_control.h
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-PID (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _MOTORS_CONTROL_H_
#define	_MOTORS_CONTROL_H_

#include <xc.h>
#include <stdint.h>
#include "config/motors_control_config.h"
#include "motors_control_impl.h"

#define MOTORS_CONTROL_BIT_PST 0x80U // preset
#define MOTORS_CONTROL_BIT_BRK 0x40U // break
#define MOTORS_CONTROL_BIT_INC 0x20U // incremental option for position
#define MOTORS_CONTROL_BIT_PWM 0x01U // pwm
#define MOTORS_CONTROL_BIT_POS 0x02U // position loop
#define MOTORS_CONTROL_BIT_VEL 0x04U // velocity loop

typedef uint8_t ControlMode;

#define MOTORS_CONTROL_MODE_NONE 0
#define MOTORS_CONTROL_MODE_BRK  MOTORS_CONTROL_BIT_BRK
#define MOTORS_CONTROL_MODE_PST  MOTORS_CONTROL_BIT_PST
#define MOTORS_CONTROL_MODE_PWM  MOTORS_CONTROL_BIT_PWM 
#define MOTORS_CONTROL_MODE_POS  (MOTORS_CONTROL_BIT_PWM|MOTORS_CONTROL_BIT_POS)
#define MOTORS_CONTROL_MODE_VEL  (MOTORS_CONTROL_BIT_PWM|MOTORS_CONTROL_BIT_VEL)

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    extern void motors_control_init(void);
    extern inline void motors_control_isr(void);
    extern void motors_control_set_command(int8_t idx, ControlMode mode, int16_t target);
    extern void motors_control_execute(void);
    extern uint16_t motors_control_get_isr_event_couter(void);
    extern uint16_t motors_control_feedback(void);
    extern uint16_t motors_control_get_cycle_of_last_command(int8_t idx);
    extern ControlMode motors_control_get_mode(int8_t idx);
    extern int16_t motors_control_get_actual_position(int8_t idx);
    extern int16_t motors_control_get_actual_velocity(int8_t idx);
    extern int16_t motors_control_get_demand_position(int8_t idx);
    extern int16_t motors_control_get_demand_velocity(int8_t idx);
    extern int16_t motors_control_get_demand_pwm(int8_t idx);
    
    extern void motors_control_set_position_parameter_error_min(int16_t min);
    extern void motors_control_set_position_parameter_error_max(int16_t max);
    extern int16_t motors_control_get_position_parameter_error_min(void);
    extern int16_t motors_control_get_position_parameter_error_max(void);
    extern void motors_control_set_position_parameter_ignore_min(int16_t min);
    extern void motors_control_set_position_parameter_ignore_max(int16_t max);
    extern int16_t motors_control_get_position_parameter_ignore_min(void);
    extern int16_t motors_control_get_position_parameter_ignore_max(void);
    extern void motors_control_set_position_parameter_hysteresis_min(int16_t min);
    extern void motors_control_set_position_parameter_hysteresis_max(int16_t max);
    extern int16_t motors_control_get_position_parameter_hysteresis_min(void);
    extern int16_t motors_control_get_position_parameter_hysteresis_max(void);
    extern void motors_control_set_position_parameter_mv_min(int16_t min);
    extern void motors_control_set_position_parameter_mv_max(int16_t max);
    extern int16_t motors_control_get_position_parameter_mv_min(void);
    extern int16_t motors_control_get_position_parameter_mv_max(void);
    extern void motors_control_set_position_parameter_gain_P(float Gp);
    extern void motors_control_set_position_parameter_gain_Ti(float Ti);
    extern float motors_control_get_position_parameter_gain_P(void);
    extern float motors_control_get_position_parameter_gain_Ti(void);
    
    extern void motors_control_set_velocity_parameter_error_min(int16_t min);
    extern void motors_control_set_velocity_parameter_error_max(int16_t max);
    extern int16_t motors_control_get_velocity_parameter_error_min(void);
    extern int16_t motors_control_get_velocity_parameter_error_max(void);
    extern void motors_control_set_velocity_parameter_ignore_min(int16_t min);
    extern void motors_control_set_velocity_parameter_ignore_max(int16_t max);
    extern int16_t motors_control_get_velocity_parameter_ignore_min(void);
    extern int16_t motors_control_get_velocity_parameter_ignore_max(void);
    extern void motors_control_set_velocity_parameter_hysteresis_min(int16_t min);
    extern void motors_control_set_velocity_parameter_hysteresis_max(int16_t max);
    extern int16_t motors_control_get_velocity_parameter_hysteresis_min(void);
    extern int16_t motors_control_get_velocity_parameter_hysteresis_max(void);
    extern void motors_control_set_velocity_parameter_mv_min(int16_t min);
    extern void motors_control_set_velocity_parameter_mv_max(int16_t max);
    extern int16_t motors_control_get_velocity_parameter_mv_min(void);
    extern int16_t motors_control_get_velocity_parameter_mv_max(void);
    extern void motors_control_set_velocity_parameter_gain_P(float Gp);
    extern void motors_control_set_velocity_parameter_gain_Ti(float Ti);
    extern float motors_control_get_velocity_parameter_gain_P(void);
    extern float motors_control_get_velocity_parameter_gain_Ti(void);

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* _MOTORS_CONTROL_H_ */

